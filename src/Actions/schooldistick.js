import {SCHOOLDISTICK_LIST, } from './Actions'
import axios from '../helpers/axiosinstance';

export const getscchoolDistickListAction = (state) => ({
    type: SCHOOLDISTICK_LIST,
    Payload: state
});


export const getschoolDitstickList = () => async dispatch => {
    try{
        const Response = await axios.get('api/schooldistick/all');
        dispatch(getscchoolDistickListAction(Response.data))
    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
} 
