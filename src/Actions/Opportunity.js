import {OPPORTUNITY_LIST, OPPORTUNITY_LIST_BYID} from './Actions'
import axios from '../helpers/axiosinstance';

export const getOpportunityListAction = (state) => ({
    type: OPPORTUNITY_LIST,
    Payload: state
});

export const getOpportunityByidListAction = (state) => ({
    type: OPPORTUNITY_LIST_BYID,
    Payload: state,
});


export const getOpportunityLis = () => async dispatch => {
    try{
        const Response = await axios.get('api/opportunity/all');
        dispatch(getOpportunityListAction(Response.data))
    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
} 

export const getOpportunityListByid = (id) => async dispatch => {
    try{
        const Response = await axios.get('api/opportunity/getOne/'+ id);
        dispatch(getOpportunityByidListAction(Response.data))

    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
}

export const findOneOpportunityId = (id) => async dispatch => {
    console.log('api/opportunity/newOpportunity::::::::::::::', id )
    try{
        const Response = await axios.get('api/opportunity/findOneOpportunityId/'+ id);
        dispatch(getOpportunityByidListAction(Response.data))

    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
}
export const findStudentId = (params) => async dispatch => {
    try{
        const Response = await axios.post('api/opportunity/findStudentId', params);
        dispatch(getOpportunityByidListAction(Response.data))

    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
}



export const createOpportunity = (params) => async dispatch => {

    try {
        const Create = await axios.post('api/opportunity/newOpportunity', params);
        console.log('Resoponse.data', Create.data);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}


export const updateOpportunity = (params) => async dispatch => {
    console.log('api/opportunity/newOpportunity::::::::::::::', params )

    try {
        const Create = await axios.post('api/opportunity/updateOpportunity', params);
        console.log('Resoponse.data', Create.data);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}