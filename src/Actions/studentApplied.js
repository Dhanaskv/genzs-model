import { STUDENTAPPLIED_LIST } from './Actions';
import axios from '../helpers/axiosinstance';
export const getAppliedDetailAction = (state) => ({
    type: STUDENTAPPLIED_LIST,
    Payload: state
});


export const getAppliedList = (id) => async dispatch => {
    try {
        const Response = await axios.get('api/studentApplied/all/' + id);
        dispatch(getAppliedDetailAction(Response.data))
    } catch (error) {
        console.log('This error is from Get Student Detail', error);
    }
}

export const newApplied = (params) => async dispatch => {
    try {
        const Create = await axios.post('api/studentApplied/newApplied', params);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}