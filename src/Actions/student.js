import { GET_STUDENT_DETAIL } from './Actions';
import axios from '../helpers/axiosinstance';
export const getStudentDetailAction = (state) => ({
    type: GET_STUDENT_DETAIL,
    Payload: state
});


export const getStudentDetail = () => async dispatch => {
    try {
        const Response = await axios.get('api/student/all');
        dispatch(getStudentDetailAction(Response.data))
    } catch (error) {
        console.log('This error is from Get Student Detail', error);
    }
}

export const getStudentById = (id) => async dispatch => {
    try {
        const Response = await axios.get('api/student/findOneStudent/' + id);
        dispatch(getStudentDetailAction(Response.data))
    } catch (error) {
        console.log('This error is from Get Student Detail', error);
    }
}

export const createNewStudent = (params) => async dispatch => {
    try {
        const Create = await axios.post('api/student/newStudent', params);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}

export const updateStudent = (params) => async dispatch => {
    console.log('the :::::::::::::::::::::::::', params)
    try {
        const Create = await axios.post('api/student/updateStudent', params);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}