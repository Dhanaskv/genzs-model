import axios from '../helpers/axiosinstance';
import * as axoiss from 'axios';
import moment from 'moment';
import authGuard from '../helpers/authGuard';
// import { AUTH_SUCCESS, AUTH_FAIL, AUTH_LOGOUT, AUTH_REQUEST_PROFILE, AUTH_RECEIVE_PROFILE } from './Actions'

import {
    LOCATION_CHANGE
} from './Actions';

const AuthService = new authGuard();
export const loginSuccess = () => ({
    type: 'AUTH_SUCCESS',
})

export const loginFailure = (data) => ({
    type: 'AUTH_FAIL',
    data
})

export const logoutAction = () => ({
    type: 'AUTH_LOGOUT'
})

const requestProfile = () => ({
    type: 'AUTH_REQUEST_PROFILE',
})

export const receiveProfile = (data) => ({
    type: 'AUTH_RECEIVE_PROFILE',
    data
});


export const setPathChange = async (history = false, optionalParams = false, crrtState = false) => {
    const formateDataPrev = (_histry) => {
        return {
            pathname: _histry.location.pathname,
            state: _histry.location.state ? { ..._histry.location.state } : {},
            optionalParams: optionalParams ? { ...optionalParams } : {}
        }
    };

    const formateDataCur = (data) => {
        return {
            pathname: data.pathname,
            state: data.state ? { ...data.state } : {},
            optionalParams: data.optionalParams ? { ...data.optionalParams } : {}
        }
    };

    const locationStatus = {
        previousState: history ? formateDataPrev(history) : {
            pathname: '/',
            state: {},
            optionalParams: {}
        },
        currentState: crrtState ? formateDataCur(crrtState) : {
            pathname: '/',
            state: {},
            optionalParams: {}
        }
    };

    await localStorage.setItem('@currentState', JSON.stringify(locationStatus));
    if (history) {
        history.push(crrtState);
    }
};

export const getCurrentState = async () => {
    const storageData = await localStorage.getItem('@currentState');
    if (storageData) {
        return JSON.parse(storageData);
    }

    return false;
};


export const AfterLoginAuth = (data, history) => dispatch => {
    console.log('this is coming from the user action', data, history)
    dispatch(loginSuccess())
    dispatch(receiveProfile(data));
    if (AuthService.setToken(data)) {
        window.location.href = data.RoleId == 1 ? '/ngo' : '/opportunity';
    }
}


export const TokenRefresh = (data) => async dispatch => {
    const response = await axios.post('/api/users/login', data)
    if (response.data.success) {
        dispatch(loginSuccess())
        dispatch(receiveProfile(response.data));
        response.data.sessionTimeOut = moment(new Date()).add(30, 'm').toDate();
        sessionStorage.setItem('UserData', JSON.stringify(response.data));
    }
}

export const verifyToken = (data, history) => async dispatch => {
    try {
        console.log('ists coming in the user verify token')
        const response = await axios.post('/api/users/login', data)
        if (response.data.success) {
            dispatch(loginSuccess())
            dispatch(receiveProfile(response.data));
            response.data.sessionTimeOut = moment(new Date()).add(30, 'm').toDate();
            sessionStorage.setItem('UserData', JSON.stringify(response.data));
        }
        // else if (response.data.hasOwnProperty('data') && response.data.data == "noaccess") {
        //     history.push('/');
        //     return response;
        // }
        else {
            history.push('/login');
            return response;
        }
    } catch (e) {
        dispatch(loginFailure(e.message))
        history.push('/login');
    }
};

export const check_token = async (req, res, next) => {

    let _responce = {
        data: {
            success: false,
        }
    };

    if (req.body.token) {
        jwt.verify(req.body.token, 'genzs', (error, decoded) => {

            if (error) {
                _responce.data.success = false;
                res.send(_responce);
            }
            else {
                _responce.data.success = true;
                res.send(_responce);
            }
        });
    }
    else {
        _responce.data.success = false;
        res.send(_responce);
    }
}


export const emailAlreadyExits = (params) => async dispatch => {
    try {
        const email_val = await axios.post('api/users/emailAlreadyExits', params);
        return email_val.data;
    } catch (error) {
        console.log('Error in email Validation ')
    }
}

