import { GET_ADMIN_BY_ID } from './Actions';
import axios from '../helpers/axiosinstance';

export const getAdminByIdAction = (state) => ({
    type: GET_ADMIN_BY_ID,
    Payload: state
})

export const getAdminById = (params) => async dispatch => {
    try {
        const Admin = await axios.post('api/admin/adminById', params);
        console.log('Admin',Admin.data)
        dispatch(getAdminByIdAction(Admin.data.data));
    } catch (error) {
        console.log('something went worng while fetching Admin by id', error);
    }

}

