import { ORGANIZATION_LIST, ORGANIZATION_BY_ID, ORGANIZATION_BY_ID_VIEW } from './Actions'
import axios from '../helpers/axiosinstance';

export const getOrganizationyListAction = (state) => ({
    type: ORGANIZATION_LIST,
    Payload: state
});

export const getOrganizationByIdAction = (state) => ({
    type: ORGANIZATION_BY_ID,
    Payload: state
})

export const getOrganizationByIdViewAction = (state) => ({
    type: ORGANIZATION_BY_ID_VIEW,
    Payload: state,
});

export const getOrganizationList = () => async dispatch => {
    try {
        const Response = await axios.get('api/organization/all');
        dispatch(getOrganizationyListAction(Response.data))
    } catch (error) {
        console.log('This error is from Get Student Detail', error);
    }
}

export const getFunOrganizationList = async dispatch => {
    try {
        const Response = await axios.get('api/organization/all');
        dispatch(getOrganizationyListAction(Response.data))
    } catch (error) {
        console.log('This error is from Get Student Detail', error);
    }
}


export const createNewOrganization = (params) => async dispatch => {
    try {
        const newOrganization = await axios.post('api/organization/createOrganization', params);
        return newOrganization.data;
    } catch (error) {
        console.log('Errro While Creating the Organization', error);
    }
}

export const getOrganizationById = async (id, dispatch) => {
    try {
        const organisationById = await axios.get('api/organization/organizationById/' + id);
        dispatch(getOrganizationByIdAction(organisationById.data.data));
    } catch (error) {
        console.log('Error while fetching organication by id', error);
    }
}

export const updatedStatusOrg = async (params, dispatch) => {
    try{
        const update = await axios.post('api/organization/updateOrgStatus', params);
        console.log('updatedata in ngo ', update, update.data);
        return update.data;
    }catch(error){
        console.log('Error while updateing the status of NGO', error);
    }
}

export const getOrganizationViewById = (id) => async dispatch => {
    console.log('the is api:::::::::::::',id)

    try{
        const Response = await axios.get('api/organization/organizationById/' + id);
        dispatch(getOrganizationByIdViewAction(Response.data.data))
    }catch(error){
        console.log('This error is from Get Student Detail', error);
    }
} 

export const updateOrganization = (params) => async dispatch => {
    console.log('the :::::::::::::::::::::::::', params)
    try {
        const Create = await axios.post('api/organization/updateOrganization', params);
        return Create.data;
    } catch (error) {
        console.log('Error while Register New user', error);
    }
}


