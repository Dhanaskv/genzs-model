import React, { Component } from "react";
import { Button, Row, Col, FormGroup, FormLabel, Breadcrumb } from 'react-bootstrap';
import { Field, reduxForm, SubmissionError, initialize,  } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea } from '../FieldComponent/Field';
import { Container } from '@material-ui/core';
import { connect } from 'react-redux';
import { emailAlreadyExits } from '../../Actions/user';
import { required, email, normalizePhone, isDOBValid1, maxLength250 } from '../../helpers/validations'
import authGuard from '../../helpers/authGuard'
import { getOrganizationViewById, updateOrganization } from '../../Actions/Organization'
import { getStudentById, updateStudent } from '../../Actions/student';


const AuthService = new authGuard()


class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: 'none',
            studentEmailValid: 'none',
            isStudent: false

        }
    }


    componentDidMount = async () => {
        let user_details = AuthService.getProfile();

        if (user_details.OrganizationId == null) {
            this.setState({ isStudent: true })
        }
        await this.props.getStudentById(user_details.StudentId)
        console.log('this:::::::::::::::::::11', this.props.studentReducer.allStudentDetail)

        if (user_details.OrganizationId != null) {
            await this.props.getOrganizationViewById(user_details.OrganizationId)
            var Organization = this.props.OrganizationReducer.organizationByIdView

            this.props.dispatch(initialize(
                'editUser',
                {
                    OrganizationName: Organization[0].Organizationname,
                    Website: Organization[0].Website,
                    OfficialEmail: Organization[0].OfficialEmail,
                    ShortSummary: Organization[0].ShortSummary,
                    AboutUs: Organization[0].AboutUs,
                    Address: Organization[0].Address,
                    FirstName: Organization[0].ContactPersonFirstName,
                    Role: Organization[0].ContactPersonRole,
                    Email: Organization[0].OfficialEmail,
                    PhoneNo: Organization[0].ContactPersonMobileNumber,


                }));

        } else {
            await this.props.getStudentById(user_details.StudentId)
            let student = this.props.studentReducer.allStudentDetail

            this.props.dispatch(initialize(
                'editUser',
                {
                    FirstName: student[0].FirstName,
                    LastName: student[0].LastName,
                    Email: student[0].Email,
                    Grade: student[0].Grade,
                    SchoolName: student[0].SchoolName,
                    DOB: student[0].DOB,
                    PhoneNo: student[0].Mobile,
                }));

        }


    };

    cancelButton = (e) => {
        this.props.history.push('/ViewProfile')

    }


    saveUser = async (value) => {
        let user_details = AuthService.getProfile();



        if (this.state.isStudent == true) {
            let values = value
            values.SchoolDistrict = user_details.SchoolDistrict
            values.Id = user_details.StudentId

            let email = value.Email.toString();
            let domainName = email.slice(email.indexOf("@") + 1, email.length);
            value.DomainName = domainName

            let response = await this.props.updateStudent(values)
            if (response.success) {
                this.props.showNotification('success', 'Registration!', `The Student Profile has been Updated successfully.`);
                this.props.history.push('/organizations')

            } else {
                if (response.emailNValid) {
                    throw new SubmissionError({
                        Email: 'Please Enter your valid School Mail ID'
                    });
                }
            }
        } else {
            let values = value
            values.Id = user_details.OrganizationId
            let response = await this.props.updateOrganization(values);
            if (response.success) {
                this.props.showNotification('success', 'Registration!', `The Organization Profile has been Updated successfully.`);
                this.props.history.push('/organizations')

            };

        };

    }

    render() {

        const { handleSubmit } = this.props;
        const { studentEmailValid, isStudent } = this.state;

        return (
            <div>
                <form onSubmit={handleSubmit(this.saveUser)}>
                    {isStudent == true ? <Container className='container-form' maxWidth="md" >
                        <Row >
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>First Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="FirstName" component={myInput} type="text" placeholder="Enter First Name" validate={required} />
                                </FormGroup>
                            </Col>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>Last Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="LastName" component={myInput} type="text" placeholder="Enter Last Name" validate={required} />
                                </FormGroup>
                            </Col>

                            {/* <Col sm="6">
                            <FormGroup>
                                <FormLabel>Password<span className="astriek"> *</span></FormLabel>
                                <Field name="Password" component={myInput} type="password" placeholder="Enter Password" validate={required, minValue8} />
                            </FormGroup>
                        </Col> */}
                        </Row>
                        <Row>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>Email<span className="astriek"> *</span></FormLabel>
                                    <Field name="Email" component={myInput} type="email" placeholder="Enter Email" validate={required, email} />
                                </FormGroup>
                            </Col>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>Grade<span className="astriek"> *</span></FormLabel>
                                    <Field name="Grade" component={myInput} type="text" placeholder="Enter Your Grade" validate={required} />
                                </FormGroup>
                            </Col>

                        </Row>
                        <Row>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>School Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="SchoolName" component={myInput} type="text" placeholder="Enter School Name" validate={required} />
                                </FormGroup>
                            </Col>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>DOB<span className="astriek"> *</span></FormLabel>
                                    <Field name="DOB" component={myInput} type="date" placeholder="Enter DOB" validate={required, isDOBValid1} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>Phone No<span className="astriek"> *</span></FormLabel>
                                    <Field name="PhoneNo" component={myInputNumberOnly} normalize={normalizePhone} type="text" placeholder="Enter Phone No" validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <div className="col text-center">
                            <Button className="mb-20" variant="primary" type="submit">
                                Update
                         </Button>
                            <Button className="mb-20" variant="secondary" onClick={this.cancelButton} >Cancel</Button>

                        </div>
                    </Container> :
                        <Container className='container-form' maxWidth='md'>
                            <h5>Organization Details</h5>
                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Organization name<span className="astriek"> *</span></FormLabel>
                                        <Field name="OrganizationName" component={myInput} type="text" placeholder="Enter Organization Name" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Website<span className="astriek"> *</span></FormLabel>
                                        <Field name="Website" component={myInput} type="text" placeholder="Enter Website" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Official Email<span className="astriek"> *</span></FormLabel>
                                        <Field name="OfficialEmail" component={myInput} type="text" placeholder="Enter Offical Email" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row >
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Short Summary<span className="astriek"> *</span></FormLabel>
                                        <Field name="ShortSummary" component={myTextArea} rows={1} type="text" placeholder="Short Summary..." validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>About us<span className="astriek"> *</span></FormLabel>
                                        <Field name="AboutUs" component={myTextArea} rows={1} type="text" placeholder="Enter About the Organization" validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Address<span className="astriek"> *</span></FormLabel>
                                        <Field name="Address" component={myTextArea} rows={2} type="text" placeholder="Enter Address" validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <h5>Contact Person Details</h5>
                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel> Name<span className="astriek"> *</span></FormLabel>
                                        <Field name="FirstName" component={myInput} type="text" placeholder="Enter First Name" validate={required} />
                                    </FormGroup>
                                </Col>
                                {/* <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Last Name<span className="astriek"> *</span></FormLabel>
                                        <Field name="LastName" component={myInput} type="text" placeholder="Enter Last Name" validate={required} />
                                    </FormGroup>
                                </Col> */}
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Role<span className="astriek"> *</span></FormLabel>
                                        <Field name="Role" component={myInput} type="text" placeholder="Enter Role" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Email<span className="astriek"> *</span></FormLabel>
                                        <Field name="Email" component={myInput} type="text" placeholder="Enter Email" validate={required, email} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row >

                                {/* <Col sm="4">
                                <FormGroup>
                                    <FormLabel>Password<span className="astriek"> *</span></FormLabel>
                                    <Field name="Password" component={myInput} type="password" placeholder="Enter  Password" validate={required, minValue8} />
                                </FormGroup>
                            </Col> */}
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Contact Number<span className="astriek"> *</span></FormLabel>
                                        <Field name="PhoneNo" component={myInputNumberOnly} normalize={normalizePhone} type="text" placeholder="Enter Contact Number" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <div className="col text-center">
                                <Button className="mb-20" variant="primary" type="submit">
                                    Update
                         </Button>
                                <Button className="mb-20" variant="secondary" onClick={this.cancelButton} >Cancel</Button>

                            </div>
                        </Container>
                    }
                </form>
                {/* </Card> */}
            </div>
        )

    }


}

const createProductForm = reduxForm({ form: 'editUser' })

const ProfileEdit = createProductForm(Edit);

const mapStateToProps = state => ({
    studentReducer: state.studentReducer,
    OrganizationReducer: state.OrganizationReducer
});

const mapDispatchToProps = {
    getOrganizationViewById,
    getStudentById,
    updateStudent,
    emailAlreadyExits,
    updateOrganization

};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEdit)