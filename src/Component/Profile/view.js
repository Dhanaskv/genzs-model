import { Card } from "@material-ui/core";
import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Button, Row, Col, FormGroup, FormLabel, Breadcrumb } from 'react-bootstrap';
import { CardBody } from 'reactstrap'
import { Field, reduxForm, change, SubmissionError, initialize } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea } from '../FieldComponent/Field';
import { Container, TextField, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { Grid } from "@material-ui/core";
import authGuard from '../../helpers/authGuard'
import dateFormat from 'dateformat'
import { getOrganizationViewById } from '../../Actions/Organization'
import { getStudentById } from '../../Actions/student';
import { getAdminById } from '../../Actions/admin';

const AuthService = new authGuard()


class View extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isStudent: false,
            studentData: [{
                Id: ''
            }],
            organizationData: [{
                Id: ''
            }],
            adminData: [],
            adminEmail: '',
            isAdmin: false
        }

    }

    componentDidMount = async () => {
        let user_details = AuthService.getProfile();

        if (user_details.OrganizationId != null) {
            await this.props.getOrganizationViewById(user_details.OrganizationId)
            var Organization = this.props.OrganizationReducer.organizationByIdView
            this.setState({ organizationData: Organization })

        } else if (user_details.AdministratorId) {
            await this.props.getAdminById({ adminId: user_details.AdministratorId });
            let admin = this.props.adminReducer.adminById;
            this.setState({ adminData: admin, adminEmail: user_details.UserName, isAdmin: true });
        } else {
            await this.props.getStudentById(user_details.StudentId)
            let student = this.props.studentReducer.allStudentDetail
            this.setState({ studentData: student, isStudent: true })

        }
    }


    cancelButton = (e) => {
        this.props.history.push('/organizations')

    }
    editButton = (e) => {
        this.props.history.push('/EditProfile')

    }


    render() {

        const { isStudent, studentData, organizationData, adminData, isAdmin, adminEmail } = this.state;
        console.log('the :::::::::::::::::::::adminData', this.props)

        return (
            <div>
                <div className="container-form">
                    {/* <label className="breadcrumb">NGO List/</label>
                <label>View Organization</label> */}
                    <Container maxWidth='lg' >
                        <Breadcrumb className="breadcrumbs-cs">
                            {!isStudent && !isAdmin && <Breadcrumb.Item active>View NGO</Breadcrumb.Item>}
                            {isStudent == true && <Breadcrumb.Item active>View Student</Breadcrumb.Item>}
                            {isAdmin && <Breadcrumb.Item active>View Admin</Breadcrumb.Item>}
                        </Breadcrumb>
                    </Container>
                    {!isStudent && !isAdmin && <Container maxWidth='md'>
                        <h3>Organization Details</h3>
                        <Row>
                            <Col md={'4'}>
                                <label><b>Organization Name</b></label>
                                <FormGroup>{organizationData[0].Organizationname}</FormGroup>
                            </Col>
                            <Col md={'4'}>
                                <label><b>Website</b></label>
                                <FormGroup>{organizationData[0].Website}</FormGroup>
                            </Col>
                            <Col md={'4'}>
                                <label><b>Official Email</b></label>
                                <FormGroup>{organizationData[0].OfficialEmail}</FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <label><b>Short Summary</b></label>
                                <FormGroup>{organizationData[0].ShortSummary}</FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <label><b>About us </b></label>
                                <FormGroup>{organizationData[0].AboutUs}</FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col >
                                <label><b>Address</b></label>
                                <FormGroup>{organizationData[0].Address}</FormGroup>
                            </Col>
                        </Row>
                        <h3>Contact Person Details</h3>
                        <Row>
                            <Col md={'4'}>
                                <label><b>Name</b></label>
                                <FormGroup>{organizationData[0].ContactPersonFirstName}</FormGroup>
                            </Col>
                            <Col md={'4'}>
                                <label><b>Role</b></label>
                                <FormGroup>{organizationData[0].Organizationname}</FormGroup>
                            </Col>
                            <Col md={'4'}>
                                <label><b>Email</b></label>
                                <FormGroup>{organizationData[0].ContactPersonEmail}</FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={'4'}>
                                <label><b>Contact Number</b></label>
                                <FormGroup>{organizationData[0].ContactPersonMobileNumber}</FormGroup>
                            </Col>
                            <Col md={'4'}>
                                <label><b>Status</b></label>
                                <FormGroup>{organizationData[0].Status}</FormGroup>
                            </Col>
                        </Row>

                        <div className="col text-center">
                            <Button className="mb-20" variant="primary" onClick={this.editButton} >Edit </Button> {' '}
                            <Button className="mb-20" variant="secondary" onClick={() => this.props.history.goBack()} > Cancel </Button>

                        </div>
                    </Container>}


                </div>


                {isStudent &&
                    <Container className='container-form' maxWidth="md" >
                        <h3>Student Details</h3>
                        <Row>
                            <Col md={'6'}>
                                <label><b>First Name</b></label>
                                <FormGroup>{studentData[0].FirstName}</FormGroup>
                            </Col>
                            <Col md={'6'}>
                                <label><b>Last Name</b></label>
                                <FormGroup>{studentData[0].LastName}</FormGroup>
                            </Col>

                        </Row>
                        <Row>
                            <Col md={'6'}>
                                <label><b>Email</b></label>
                                <FormGroup>{studentData[0].Email}</FormGroup>
                            </Col>
                            <Col md={'6'}>
                                <label><b>Grade</b></label>
                                <FormGroup>{studentData[0].Grade}</FormGroup>
                            </Col>

                        </Row>
                        <Row>
                            <Col md={'6'}>
                                <label><b>School Name</b></label>
                                <FormGroup>{studentData[0].SchoolName
                                }</FormGroup>
                            </Col>

                            <Col xs sm="6">
                                <FormGroup>
                                    <label><b>DOB</b></label><br></br>
                                    <FormLabel>{studentData[0].DOB}<span className="astriek"> *</span></FormLabel>
                                </FormGroup>
                            </Col>

                        </Row>
                        <Row>


                            <Col sm="6">
                                <FormGroup>
                                    <label><b>Phone No</b></label><br></br>
                                    <FormLabel>{studentData[0].Mobile}<span className="astriek"> </span></FormLabel>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>

                        </Row>
                        <div className="col text-center">
                            <> <Button className="mb-20" variant="primary" onClick={this.editButton} >Edit </Button> {' '}
                                <Button className="mb-20" variant="secondary" onClick={this.cancelButton} > Cancel </Button> </>

                        </div>
                    </Container>
                }
                {isAdmin &&
                    <Container className='container-form' maxWidth="md" >
                        <h3>Profile Details</h3>
                        <Row>
                            <Col md={'6'}>
                                <label><b>First Name</b></label>
                                <FormGroup>{adminData[0].FirstName}</FormGroup>
                            </Col>
                            <Col md={'6'}>
                                <label><b>Last Name</b></label>
                                <FormGroup>{adminData[0].LastName}</FormGroup>
                            </Col>

                        </Row>
                        <Row>
                            <Col md={'6'}>
                                <label><b>Email</b></label>
                                <FormGroup>{adminEmail}</FormGroup>
                            </Col>
                        </Row>
                        <div className="col text-center">
                            <Button className="mb-20" variant="primary" onClick={this.editButton} >Edit </Button> {' '}
                            <Button className="mb-20" variant="secondary" onClick={this.cancelButton} > Cancel </Button>

                        </div>
                    </Container>


                }

            </div>
        )
    }
}



const mapStateToProps = state => ({
    studentReducer: state.studentReducer,
    OrganizationReducer: state.OrganizationReducer,
    adminReducer: state.adminReducer

});

const mapDispatchToProps = {
    getStudentById,
    getOrganizationViewById,
    getAdminById
};

export default connect(mapStateToProps, mapDispatchToProps)(View);