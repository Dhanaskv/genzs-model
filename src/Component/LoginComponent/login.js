import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Avatar, Button, CssBaseline, TextField, Container, Typography, Box, Grid, Link, FormControlLabel, Checkbox } from "@material-ui/core"
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import axios from '../../helpers/axiosinstance';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function LogIn() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const history = useHistory();
    const [userName, setUserName] = useState('');
    const [password, setPassWord] = useState('');
    const [userNameReq, setUserNameReq] = useState(false);
    const [passwordReq, setPasswordReq] = useState(false);
    const [commonError, setCommonError] = useState('none');
    const [errorMessage, setErrorMessage] = useState('Invalid username or password');

    const handleChange = (type, value) => {
        if (type === "email") {
            setUserName(value.target.value);
        } else {
            setPassWord(value.target.value);
        }
    }
    const user_login = async (e) => {
        e.preventDefault();
        if (userName == '') setUserNameReq(true);
        if (password == '') setPasswordReq(true);

        if (userName != '' && password != '') {
            setUserNameReq(false);
            setPasswordReq(false);
            const verificationData = await axios.post('/api/users/login', {
                uName: userName,
                password: password
            });
            if (verificationData.data.success) {
                console.log('verfication... num num', verificationData.data.data)
                // localStorage.setItem('userloggedin', JSON.stringify(verificationData.data.data));
                history.push('/opportunitiy');
            } else {
                setCommonError('block');
                setErrorMessage(verificationData.data.message);
            }
        }
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar} >
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Log In
                 </Typography>
                <form className={classes.form} method="POST" onSubmit={user_login}>
                    <div className="error_login" style={{ display: commonError }}>{errorMessage}</div>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="email"
                        label="Email Address"
                        // name="email"
                        // autoComplete="email"
                        error={userNameReq ? true : false}
                        helperText={userNameReq ? "Email Address is Required" : ''}
                        autoFocus
                        onChange={(value) => handleChange('email', value)}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        // required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        error={passwordReq ? true : false}
                        helperText={passwordReq ? "Password is Required" : false}
                        onChange={(value) => handleChange('Password', value)}
                    />
                    {/* <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          /> */}
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Log In
          </Button>
                    {/* <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid> */}
                </form>
            </div>
            <Box mt={8}>
            </Box>
        </Container>
    );
}