import React, { Component } from 'react';
import { Avatar, Button, CssBaseline, TextField, Container, Typography, Box, Grid, Link, FormControlLabel, Checkbox } from "@material-ui/core"
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import axios from '../../helpers/axiosinstance';
import { connect } from 'react-redux';
import { AfterLoginAuth, verifyToken, loginSuccess, } from '../../Actions/user';
import authGuard from '../../helpers/authGuard';
import Loader from '../../helpers/loader';
const AuthService = new authGuard();

class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            studentEmailValid: 'none',
            userName: '',
            password: '',
            userNameReq: false,
            passwordReq: false,
            commonError: 'none',
            errorMessage: 'Invalid username or password',
            loading: true
        }
        this.handleChange = this.handleChange.bind(this);
    };

    componentDidMount = async () => {

        console.log('this.props.props', this.props)
    };

    handleChange = (type, value) => {
        if (type === "email") {
            this.setState({ userName: value.target.value });
        } else {
            this.setState({ password: value.target.value });
        }
    }

    user_login = async (e) => {
        const { userName, password } = this.state;
        console.log('this.name')
        e.preventDefault();
        if (userName == '') this.setState({ userNameReq: true });
        if (password == '') this.setState({ passwordReq: true });

        if (userName != '' && password != '') {
            this.setState({ loader: true });
            this.setState({ userNameReq: false });
            this.setState({ passwordReq: false });
            const verificationData = await axios.post('/api/users/login', {
                uName: userName,
                password: password
            });
            if (verificationData.data.success) {
                this.props.AfterLoginAuth(verificationData.data.data, this.props.history)
            } else {
                this.setState({ commonError: 'block' });
                this.setState({ errorMessage: verificationData.data.message });
                this.setState({ loader: false });
            }
        }
    }

    validateLogin = (async () => { return await AuthService.getProfile() });

    render() {
        const { commonError, errorMessage, userNameReq, passwordReq, loader, loading } = this.state;
        return (
            <>
                { loader ? <Loader />
                    : <Container component="main" maxWidth="xs">
                        <CssBaseline />
                        <div className={'login_cointainer'}>
                            <Avatar className={'makeStyles-avatar-2'} >
                                <LockOutlinedIcon />
                            </Avatar>
                            <Typography component="h1" variant="h5">
                                Log In
                       </Typography>
                            <form className={'makeStyles-form-3 '} method="POST" onSubmit={(e) => this.user_login(e)}>
                                <div className="error_login" style={{ display: commonError }}>{errorMessage}</div>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    // name="email"
                                    // autoComplete="email"
                                    error={userNameReq ? true : false}
                                    helperText={userNameReq ? "Email Address is Required" : ''}
                                    autoFocus
                                    onChange={(value) => this.handleChange('email', value)}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    // required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    error={passwordReq ? true : false}
                                    helperText={passwordReq ? "Password is Required" : false}
                                    onChange={(value) => this.handleChange('Password', value)}
                                />
                                {/* <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                      /> */}
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={'makeStyles-submit-4'}
                                >
                                    Log In
                      </Button>
                                {/* <Grid container>
                        <Grid item xs>
                          <Link href="#" variant="body2">
                            Forgot password?
                          </Link>
                        </Grid>
                        <Grid item>
                          <Link href="#" variant="body2">
                            {"Don't have an account? Sign Up"}
                          </Link>
                        </Grid>
                      </Grid> */}
                            </form>
                        </div>
                        <Box mt={8}>
                        </Box>
                    </Container>
                }
            </>
        )
    }
};

const mapStateToProps = state => ({
    authReducer: state.authReducer
});

const mapDispatchToProps = {
    AfterLoginAuth,
    verifyToken,
    loginSuccess
};

export default connect(mapStateToProps, mapDispatchToProps)(LogIn)