import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // State 
        }
    };

    render() {
        return (
            <div>
                <Header />
                {/* About */}
                <section className="about full-screen d-lg-flex justify-content-center align-items-center" id="about">
                    <div className="container">
                        <div className="row">

                            <div className="col-lg-7 col-md-12 col-12 d-flex align-items-center">
                                <div className="about-text">
                                    <small className="small-text">Welcome to <span className="mobile-block">GENZS!</span></small>
                                    <h1 className="animated animated-text">
                                        <span className="mr-2">Are you </span>
                                        <div className="animated-info">
                                            <span className="animated-item">a Student</span>
                                            <span className="animated-item">an Organization</span>
                                        </div>
                                    </h1>

                                    <p>We help to find the volunteers for you.</p>

                                    <div className="custom-btn-group mt-4">
                                        <a href="#" className="btn mr-lg-2 custom-btn"><i className='uil uil-file-alt'></i> I'm a Student</a>
                                        <a href="#contact" className="btn custom-btn custom-btn-bg custom-btn-link">We are an organization</a>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-5 col-md-12 col-12">
                                <div className="about-image svg">
                                    <img src={require("../../assets/images/img1.png")} className="img-fluid" alt="svg image" />
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                {/* About Us */}
                <section className="aboutus py-5" id="aboutus">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-12">
                                <h2 className="mb-4">About GenZS</h2>
                                <p>Proin ornare non purus ut rutrum. Nulla facilisi. Aliquam laoreet libero ac pharetra feugiat. Cras ac fermentum nunc, a faucibus nunc.</p>
                            </div>

                            <div className="col-lg-5 col-12">

                                <img src={require("../../assets/images/logo.svg")} className="img-fluid" alt="svg image" />
                            </div>
                        </div>
                    </div>
                </section>

                {/* FEATURES */}

                <section className="services py-5 d-lg-flex justify-content-center align-items-center" id="services">
                    <div className="container">
                        <div className="row">

                            <div className="col-lg-6 col-12">
                                <h2 className="mb-4">Student</h2>

                                <div className="timeline">
                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>1</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Singup</span><small></small></h3>
                                            <p>Proin ornare non purus ut rutrum. Nulla facilisi. Aliquam laoreet libero ac pharetra feugiat. Cras ac fermentum nunc, a faucibus nunc.</p>
                                        </div>
                                    </div>

                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>2</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Search Opportunity</span><small></small></h3>
                                            <p>Fusce rutrum augue id orci rhoncus molestie. Nunc auctor dignissim lacus vel iaculis.</p>
                                        </div>
                                    </div>

                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>3</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Choose Opportunity</span></h3>
                                            <p>Sed fringilla vitae enim sit amet cursus. Sed cursus dictum tortor quis pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                        </div>
                                    </div>

                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>4</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Start Volunteering<small></small></span></h3>
                                            <p>Cras scelerisque scelerisque condimentum. Nullam at volutpat mi. Nunc auctor ipsum eget magna consequat viverra.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div className="col-lg-6 col-12">
                                <h2 className="mb-4 mobile-mt-2">Orgnaization</h2>

                                <div className="timeline">
                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>1</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Singup</span><small></small></h3>
                                            <p>Please tell your friends about Tooplate website. That would be very helpful. We need your support.</p>
                                        </div>
                                    </div>

                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>2</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Post Opportunity</span><small></small></h3>
                                            <p><a rel="nofollow" href="https://www.facebook.com/tooplate">Tooplate</a> is a great website to download HTML templates without any login or email.</p>
                                        </div>
                                    </div>

                                    <div className="timeline-wrapper">
                                        <div className="timeline-yr">
                                            <span>3</span>
                                        </div>
                                        <div className="timeline-info">
                                            <h3><span>Manage Opportunity</span><small></small></h3>
                                            <p>You can freely use Tooplate's templates for your business or personal sites. You cannot redistribute this template without a permission.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </section>

                {/* CONTACT */}
                <section className="contact py-5" id="contact">
                    <div className="container">
                        <div className="row">

                            <div className="col-lg-5 mr-lg-5 col-12">
                                <div className="google-map w-100">
                                    {/* <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12088.558402180099!2d-73.99373482142036!3d40.75895421922642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25855b8fb3083%3A0xa0f9aef176042a5c!2sTheater+District%2C+New+York%2C+NY%2C+USA!5e0!3m2!1sen!2smm!4v1549875377188" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> -->
              <iframe src="https://maps.google.com/maps?q=bellevue&t=&z=13&ie=UTF8&iwloc=&output=embed" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> --> */}

                                </div>

                                <div className="contact-info d-flex justify-content-between align-items-center py-4 px-lg-5">
                                    <div className="contact-info-item">
                                        <h3 className="mb-3 text-white">Say hello</h3>
                                        <p className="footer-text mb-0">010 020 0960</p>
                                        <p><a href="mailto:hello@company.co">hello@company.co</a></p>
                                    </div>

                                    <ul className="social-links">
                                        <li><a href="#" className="uil uil-dribbble" data-toggle="tooltip" data-placement="left" title="Dribbble"></a></li>
                                        <li><a href="#" className="uil uil-instagram" data-toggle="tooltip" data-placement="left" title="Instagram"></a></li>
                                        <li><a href="#" className="uil uil-youtube" data-toggle="tooltip" data-placement="left" title="Youtube"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-6 col-12">
                                <div className="contact-form">
                                    <h2 className="mb-4">Interested to work together? Let's talk</h2>

                                    <form action="" method="get">
                                        <div className="row">
                                            <div className="col-lg-6 col-12">
                                                <input type="text" className="form-control" name="name" placeholder="Your Name" id="name" />
                                            </div>

                                            <div className="col-lg-6 col-12">
                                                <input type="email" className="form-control" name="email" placeholder="Email" id="email" />
                                            </div>

                                            <div className="col-12">
                                                <textarea name="message" rows="6" className="form-control" id="message" placeholder="Message"></textarea>
                                            </div>

                                            <div className="ml-lg-auto col-lg-5 col-12">
                                                <input type="submit" className="form-control submit-btn" value="Send Button" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                    </section>

                    <Footer />
                            </div>
        )
    }
}

export default Home;