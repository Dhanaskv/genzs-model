import React, { Component } from 'react';
import { Nav, NavItem, NavLink, NavDropdown, Modal } from 'react-bootstrap'
import Registration from '../Registration/Registration';
import Edit from '../Profile/edit';
import View from '../Profile/view';
import { CustomModal, Content } from '../../helpers/reusable/CustomModal'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import authGuard from '../../helpers/authGuard';
import Headroom from "headroom.js";

const AuthService = new authGuard();

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            popUpIsTrue: false,
            isStudentReg: false,
            StudentLog: false,
            NgoLog: false,
            AdminLog: false,
            popUpProfile: false,
            StudentName: '',
            AdminName: ''
        };
    };

    RegisterUser = (type) => {

        if (type == 'student') {
            this.setState({ popUpIsTrue: true, isStudentReg: true });
        } else {
            this.setState({ popUpIsTrue: true, isStudentReg: false });
        }
    };

    profileViewEdit = (type) => {
        console.log('the valu of :::::::::::::::::::::::', type)

        if (type == 'Edit') {
            this.setState({ popUpProfile: true, isStudentReg: true });
        } else {
            this.setState({ popUpProfile: true, isStudentReg: false });
        }
    };
    componentDidMount = () => {

        console.log('userloggedin', AuthService.getProfile());
        let user_details = AuthService.getProfile();
        if (user_details) {
            return (
                <>
                    {user_details.RoleId == 1 && this.setState({ AdminLog: true, AdminName: user_details.FirstName })}
                    {user_details.RoleId == 2 && this.setState({ NgoLog: true })}
                    {user_details.RoleId == 3 && this.setState({ StudentLog: true, StudentName: user_details.FirstName })}
                </>
            )

        };
        const header = document.querySelector(".navbar");
        const headroom = new Headroom(header);
        headroom.init();
    }

    handleClose = () => {
        this.setState({ popUpIsTrue: false, isStudentReg: false, popUpProfile: false })
    }

    logout = () => {
        this.setState({ StudentLog: false });
        window.location.href = '/'
        AuthService.logout()
    }

    render() {
        const { popUpIsTrue, isStudentReg, StudentLog, NgoLog, AdminLog, popUpProfile, StudentName, AdminName } = this.state;
        const { location } = this.props;
        return (
            <div>

                <Nav activeKey={location.pathname + location.hash} className="navbar navbar-expand-sm navbar-light">
                    <div className="container">

                        <a className="navbar-brand" href="/"><img height="50px" width="50px" src={require('../../assets/images/logo.png')} />GENZS</a>

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                            <span className="navbar-toggler-icon"></span>
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav mx-auto">
                                <NavItem >
                                    {StudentLog || NgoLog || AdminLog ? '' : <NavLink href="/" ><span data-hover="Home">Home</span></NavLink>}
                                </NavItem>
                                {StudentLog || NgoLog || AdminLog ? '' : <NavItem >
                                    <NavLink href="/#aboutus" ><span data-hover="About us">About us</span></NavLink>
                                </NavItem>}
                                <NavItem >
                                    {(NgoLog || StudentLog) && <NavLink href="/opportunity" >{NgoLog ? <span data-hover="Manage opportunties">Manage opportunties</span> : <span data-hover="Open Volunteer Opportunities">Open Volunteer Opportunities</span>}</NavLink>
                                    }
                                </NavItem>
                                <NavItem >
                                    {AdminLog && <NavLink href="/organizations"><span data-hover="NGO List">NGO List</span></NavLink>}
                                </NavItem>
                                {StudentLog || NgoLog || AdminLog ? '' : <NavItem >
                                    <NavLink href="/#contact" ><span data-hover="Contact">Contact</span></NavLink>
                                </NavItem>}
                                {StudentLog || NgoLog || AdminLog ? '' : <NavItem >
                                    <NavDropdown title="Register">
                                        {/* <button className="nav-link button-style-none" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span data-hover="Register">Register</span> </button> */}
                                        {/* <div className="dropdown-menu" aria-labelledby="dropdownMenuButton"> */}
                                        <NavDropdown.Item className="dropdown-item button-style-none" onClick={() => this.RegisterUser('student')}>Register As Student</NavDropdown.Item>
                                        <NavDropdown.Item className="dropdown-item button-style-none" onClick={() => this.RegisterUser('ngo')}>Register As NGO</NavDropdown.Item>
                                        {/* </div> */}
                                    </NavDropdown>

                                </NavItem>}
                                {(NgoLog || StudentLog || AdminLog) && <NavItem >
                                    <NavDropdown title="Profile">
                                        {
                                            <>
                                                {StudentLog && <NavLink href="/ViewProfile" ><span data-hover="View Profile">{StudentName}</span> </NavLink>}
                                                {AdminLog && <NavLink href="/ViewProfile" ><span data-hover={`${AdminName}`} >{AdminName}</span> </NavLink>}
                                            </>
                                        }
                                        {(StudentLog || NgoLog || AdminLog) && <NavLink className="nav-link button-style-none" onClick={this.logout}><span data-hover="Logout">Logout</span></NavLink>}
                                    </NavDropdown>

                                </NavItem>}
                                <NavItem >
                                    {StudentLog || NgoLog || AdminLog ? '' :
                                        <NavLink href="/Login" ><span data-hover="Login">Login</span></NavLink>}

                                </NavItem>
                            </ul>
                        </div>
                    </div>
                </Nav>
                <div>
                    <CustomModal open={popUpIsTrue} className={!isStudentReg ? "model-hieght" : ''} onClose={this.handleClose} maxWidth={"md"} >
                        <Modal.Header style={{ borderBottom: 'none' }}>
                            <Modal.Title className="header-title moodel-header"> {isStudentReg ? <h5 >Student Register</h5> : <h5>Organization Register</h5>}</Modal.Title>
                            <HighlightOffIcon className='close-button' onClick={this.handleClose} />
                        </Modal.Header>
                        <Content>
                            <Registration {...this.props} isStudent={isStudentReg ? true : false} handleClose={this.handleClose} />
                        </Content>
                    </CustomModal>

                </div>

                {/* <div>
                    <CustomModal open={popUpProfile} className={!isStudentReg ? "model-hieght" : ''} onClose={this.handleClose} maxWidth={"md"} >
                        <Modal.Header style={{ borderBottom: 'none' }}>
                        {isStudentReg == true &&  <Modal.Title className="header-title moodel-header">   </Modal.Title>}
                        {isStudentReg == false &&  <Modal.Title className="header-title moodel-header">  </Modal.Title>}
                            <HighlightOffIcon className='close-button' onClick={this.handleClose} />
                        </Modal.Header>
                        
                        <Content>
                            {isStudentReg == true && <Edit {...this.props} isStudent={isStudentReg ? true : false} popUpProfile={popUpProfile} handleClose={this.handleClose} />}
                            {isStudentReg == false && <View {...this.props} isStudent={isStudentReg ? true : false} popUpProfile={popUpProfile} handleClose={this.handleClose} />}
                        </Content>
                    </CustomModal> */}

                {/* </div> */}
                {/* <div>
                    <CustomModal open={popUpProfile} className={!isStudentReg ? "model-hieght" : ''} onClose={this.handleClose} maxWidth={"md"} >
                        <Modal.Header style={{ borderBottom: 'none' }}>
                            <Modal.Title className="header-title moodel-header"> {isStudentReg ? <h5 > Edit</h5> : <h5>Organization Edit</h5>}</Modal.Title>
                            <HighlightOffIcon className='close-button' onClick={this.handleClose} />
                        </Modal.Header>
                        <Content>
                            {isStudentReg == true && <Edit {...this.props} isStudent={isStudentReg ? true : false} popUpProfile={popUpProfile} handleClose={this.handleClose} />}
                            {isStudentReg == true && <View {...this.props} isStudent={isStudentReg ? true : false} popUpProfile={popUpProfile} handleClose={this.handleClose} />}
                        </Content>
                    </CustomModal>

                </div> */}
            </div >
        )
    }
}

export default Header;