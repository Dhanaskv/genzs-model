import React, { Component, useState } from 'react';
import { Grid, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import { Field, reduxForm, SubmissionError, initialize, } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea } from '../FieldComponent/Field';
import { required, email, minValue8, normalizePhone, isDOBValid1, maxLength250 } from '../../helpers/validations'
import { connect } from 'react-redux';
import { getOpportunityLis, getOpportunityListByid, findOneOpportunityId, findStudentId } from '../../Actions/Opportunity'
import { CustomModal, Content } from '../../helpers/reusable/CustomModal'
import { Modal, Row, Col, FormGroup } from 'react-bootstrap'
import EditOpportunity from '../Opportunity/editOpportunity';
import authGuard from '../../helpers/authGuard'
import { getAppliedList } from '../../Actions/studentApplied';
import Autocomplete from 'react-autocomplete';
import { CardBody } from 'reactstrap'
import Loader from '../../helpers/loader'





const AuthService = new authGuard()


class opportunitiy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            opportunitiy: {
                OpportunitySubject: ''
            },
            posts: [],
            OrganizationId: [],
            popUpIsTrue: false,
            StudentLog: false,
            Id: 0,
            filteredRows: [],
            globalSearch: '',
            Status: ''

        }

    }


    addOpportunitiy = (e) => {
        this.props.history.push('/Addopportunity')

    }

    EditOpportunitiy = (id) => {

        this.props.history.push({
            pathname: '/Editopportunity',
            state: { Id: id }
        });

    }

    ViewOpportunitiy = (id) => {
        this.props.history.push({
            pathname: '/Viewopportunity',
            state: { Id: id }
        });

    }


    handleClose = () => {
        this.setState({ popUpIsTrue: false })
    }
    componentDidMount = async () => {
        this.setState({ loader: true })
        await this.props.getOpportunityLis()
        var Opportunity = this.props.OpportnuityReducer;
        let user_details = AuthService.getProfile();

        if (user_details) {
            if (user_details.RoleId == 2 || user_details.RoleId == 1) {
                this.setState({ StudentLog: true });
            }
        }
        var id = user_details.OrganizationId

        if (user_details.RoleId == 2) {
            await this.props.findOneOpportunityId(id)
        } else {
            let params = {
                SchoolDistrict: user_details.SchoolDistrict,
                studentAge: user_details.studentAge
            }
            await this.props.findStudentId(params)
            let  organizationData = this.props.OpportnuityReducer.OpportnuityListByid[0].Id
            await this.props.getAppliedList(organizationData);
          let  appliedStudent = this.props.appliedReducer.allAppliedlist[0].Status
          this.setState({Status:appliedStudent })
            console.log('the:::::::::::::::::::::::::::;organizationData',Status)
        }
        this.setState({ loader: false })

    }


    static getDerivedStateFromProps = (nextProps, prevState) => {

        let user_details = AuthService.getProfile();
        let OpportnuityData = []
        let organizationData = []

        if (user_details.RoleId == 2 || user_details.RoleId == 3) {
            OpportnuityData = nextProps.OpportnuityReducer.OpportnuityListByid || [];
            organizationData = nextProps.OpportnuityReducer.OpportnuityListByid || [];
        } else {
            OpportnuityData = nextProps.OpportnuityReducer.OpportnuityList || [];
        }
        let OpportnuityDataList = [];
        if (OpportnuityData.length > 0) {
            OpportnuityDataList = OpportnuityData.map((data) => {
                return ({
                    ...data,
                })
            })
        }


        if (prevState.posts.length != OpportnuityDataList.length) {
        };



        return ({ posts: OpportnuityDataList });

    }


    confirmUpdate = (values) => {
        // this.setState({ loader: 'block' })
        // const actionData = this.props.VIN_BD_Invoice_List(values.VIN).then(result => {
        //     this.setState({
        //         loader: 'none',
        //         tableComponentGuid: GetNewGuid()
        //     })
        // })
    }

    render() {
        const { posts, popUpIsTrue, StudentLog, filteredRows, globalSearch, Status, loader } = this.state;
        const { handleSubmit } = this.props;


        let More = "View"
        let MoreView = "View & Apply"


        return (
            <div>
                {loader ? <Loader /> :
                <div style={{ marginLeft: 10, padding: 10 }}>
                    {StudentLog == true &&
                        <Button onClick={this.addOpportunitiy} style={{ marginLeft: '90%', backgroundColor: '#00BFFF', width: '90px', height: '40px', borderRadius: '5px', color: 'white' }}>Add</Button>
                    }
                    <Grid container spacing={70} justify="left" >

                        <Card className="mb-5px">
                            <CardBody className="max-lg-pt-15px sm-pt-10px" style={{ padding: '0px' }}>
                                {/* <form onSubmit={handleSubmit}>
                                <div>
                                    <label htmlFor="firstName">First Name</label>
                                    <Field name="firstName" component={myInput} type="text" />
                                </div>
                                <div>
                                    <label htmlFor="lastName">Last Name</label>
                                    <Field name="lastName" component="input" type="text" />
                                </div>
                                <button type="submit">Submit</button>
                            </form> */}

                            </CardBody>
                        </Card>


                        {posts.map(post => (
                            <Grid item key={post.title} color='primary' width='400px' style={{ marginTop: 10, padding: 10, width: '470px' }}  >

                                <div style={{ borderLeft: (post.Status == 1) ? '6px solid #008000' : (post.Status == 2) ? '6px solid #FF0000' : '6px solid #FFA500', height: 'auto', }} key={post} >
                                    <Card style={{ height: '250px' }} >
                                        <CardActionArea onClick={this.ViewOpportunitiy.bind(this, post.Id)}>
                                            <CardContent >

                                                <Typography className={'single-line-trimp'} gutterBottom variant="h7" component="h5" color='black' >
                                                    <b>{post.OpportunitySubject}</b>
                                                </Typography>
                                                <Typography gutterBottom color='black'>
                                                    <b> {post.Organizationname}</b>
                                                </Typography>
                                                <hr />
                                                {post.OpportunityDescription.length < 160 &&
                                                    <Typography component="p">{post.OpportunityDescription}</Typography>
                                                }
                                                {post.OpportunityDescription.length > 160 &&
                                                    <Typography className={'block-with-text'} component="p">{post.OpportunityDescription}</Typography>
                                                }

                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button onClick={this.ViewOpportunitiy.bind(this, post.Id)} size="small" color="primary" justify="right">
                                                {StudentLog == true &&
                                                    More
                                                }
                                                {StudentLog == false &&
                                                    MoreView
                                                }

                                            </Button>
                                            {StudentLog == true &&
                                                <Button onClick={this.EditOpportunitiy.bind(this, post.Id)} size="small" color="primary" justify="right">
                                                    Mange
                                              </Button>

                                            }

                                            {/* <Typography gutterBottom color='black' style={{ marginLeft: '50%', backgroundColor: color, width: '90px', height: '40px', borderRadius: '5px', color: 'white' }}>
                                                <b> </b>
                                            </Typography> */}

                                            {StudentLog == true &&
                                                <Typography className={'status-bar'} style={{ marginLeft: '48%', backgroundColor: 'white', width: '90px', height: '40px', borderRadius: '5px', paddingTop: '9px', paddingLeft: '24px', color: (post.Status == 1) ? 'green' : (post.Status == 2) ? 'red' : 'Orange' }}><b>{(post.Status == 1) ? 'Active' : (post.Status == 2) ? 'Expired' : 'Draft'}</b></Typography>

                                            }
                                            {StudentLog == false &&
                                                <Typography className={'status-bar'} style={{ marginLeft: '48%', backgroundColor: 'white', width: '90px', height: '40px', borderRadius: '5px', paddingTop: '9px', paddingLeft: '24px', color: (Status == 1) ? 'green' : (Status == 2) ? 'red' : 'Orange' }}><b>{(Status == 1) ? 'Approved' : (Status == 2) ? 'rejected' : 'Applied'}</b></Typography>

                                            }

                                            {/* {StudentLog == false &&
                                                <Typography component="p" style={{ marginLeft: '58%', backgroundColor: 'white', width: '90px', height: '40px', borderRadius: '5px', paddingTop: '9px', paddingLeft: '24px' color: (posts.Status == 1) ? 'green' : (posts.Status == 2) ? 'red' : 'Orange' }}><b>{(post.Status == 1) ? 'Active' : (post.Status == 2) ? 'Expired' : 'Draft'}</b></Typography>

                                            } */}




                                        </CardActions>
                                    </Card>
                                </div>



                            </Grid>
                        ))}

                        {posts.length == 0 &&
                            <div style={{ textAlign: 'center', marginLeft: '600px' }}>
                                <p style={{ textAlign: 'center' }}><h5><b>Oops! No Opportunitiy Available </b> </h5></p>

                            </div>

                        }

                    </Grid>
                </div>
    }

            </div>
        )
    }

}


const mapStateToProps = state => ({
    OpportnuityReducer: state.OpportnuityReducer,
    appliedReducer: state.appliedReducer

});

const mapDispatchToProps = {
    getOpportunityLis,
    getOpportunityListByid,
    findOneOpportunityId,
    findStudentId,
    getAppliedList

};

export default connect(mapStateToProps, mapDispatchToProps)(opportunitiy)


