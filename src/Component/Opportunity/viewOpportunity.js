
import { Card } from "@material-ui/core";
import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Button, Row, Col, FormGroup, FormLabel } from 'react-bootstrap';
import { CardBody } from 'reactstrap'
import { Field, reduxForm, change, SubmissionError, initialize } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea } from '../FieldComponent/Field';
import { Container, TextField, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { Grid } from "@material-ui/core";
import { getOpportunityListByid, updateOpportunity } from '../../Actions/Opportunity';
import MUIDataTable from "mui-datatables";
import authGuard from '../../helpers/authGuard'
import {  updateStudent } from '../../Actions/student';
import { getAppliedList, newApplied } from '../../Actions/studentApplied';
import dateFormat from 'dateformat'

const AuthService = new authGuard()

// const columns = ["FirstName", "Grade", "Distick", "Mobile", "School Name", "Age"];

// const data = [
//     ["Joe James", "12", "Yonkers", "989909889", "GHSS", "17"],
//     ["John Walsh", "10", "Hartford", "989909889", "RPS", "15"],
//     ["Bob Herm", "11", "Tampa", "989909889", "SVVM", "16"],
//     ["James Houston", "12", "Dallas", "989909889", "SVWS", "17"],
// ];

const options = {
    filterType: 'customSearch',
    print: false,
    viewcolumns: false,
};




class View extends Component {
    constructor(props) {
        super(props);
        this.state = {

            columns: [
                {
                    name: "FirstName",
                    label: "Name",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },
                {
                    name: "Grade",
                    label: "Grade",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },
                {
                    name: "Email",
                    label: "Email",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },
                {
                    name: "Mobile",
                    label: "Phone",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },
                {
                    name: "SchoolName",
                    label: "School Name",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },
                {
                    name: "studentAge",
                    label: "Age",
                    options: {
                        sort: true,
                        searchable: false,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },

                {
                    name: "UpdatedOn",
                    label: "Applied On",
                    options: {
                        sort: true,
                        searchable: true,
                        filter: true,
                        // setCellHeaderProps: applyTableWith(110),
                    }
                },

            ],

            loader: 'none',
            Opportunitydata: [{
                OpportunitySubject: ''

            }],
            StudentId: 0,
            statusId: 0,
            Id: 0,
            rows: [],

        }
    }
    componentDidMount = async () => {
        let user_details = AuthService.getProfile();
        
        console.log("the value oof :::::::::::::::", user_details)
        let opportunityID = this.props.location.state.Id
        await this.props.getAppliedList(opportunityID);
        let appliedStudent = this.props.studentReducer.allStudentDetail

        await this.props.getOpportunityListByid(opportunityID)
        var Opportunity = this.props.OpportnuityReducer.OpportnuityListByid;

        this.setState({ Opportunitydata: Opportunity, StudentId: user_details.RoleId, statusId: Opportunity[0].Status, Id: Opportunity[0].Id, rows: appliedStudent });
    }




    static getDerivedStateFromProps = (nextProps, prevState) => {
        let appliedStudent = nextProps.appliedReducer.allAppliedlist || [];
        let studentDataList = [];

        if (appliedStudent.length > 0) {
            studentDataList = appliedStudent.map((data) => {
                return ({
                    ...data,
                    UpdatedOn: dateFormat(data.UpdatedOn, 'mmm-dd-yyyy')
                })
            })
        }
        return ({ rows: studentDataList });
    }
    cancelButton = (e) => {
        this.props.history.push('/opportunity')

    }
    EditOpportunitiy = (id) => {
        this.props.history.push({
            pathname: '/Editopportunity',
            state: { Id: id }
        });

    }

    updateOpportunity = async () => {


        let values = {
            Status: { label: "Active", value: 1 },
            Id: this.state.Id,
            OpportunityDistrict: { value: data[0].OpportunityDistrict }
        }

        
        let response = await this.props.updateOpportunity(values)

        if (response.success) {
            this.props.showNotification('success', 'Registration!', `The Opportunity  has been Published successfully.`);
            this.props.history.push('/opportunity')
        }

    }

    applyStudent = async () => {
        var data = this.state.Opportunitydata
        let user_details = AuthService.getProfile();

        let values = {
            OpportunityId: data[0].Id,
            StudentId: user_details.StudentId,
            Status: 1
        }

        let response = await this.props.newApplied(values)

        if (response.success) {
            this.props.showNotification('success', 'Registration!', ` successfully Applied For The Opportunity.`);
            this.props.history.push('/opportunity')
        }

    }

    render() {
        const { Opportunitydata, StudentId, statusId, rows, columns } = this.state
        let postion = []
        let length = rows.length
        let Status = ''

        rows.forEach(element => {
            if(element.Status == 1){
                postion.push({ element })

            }
        });
        if(rows.length > 0){ 
            Status = rows[0].Status
        }
       


        return (
            <div className="content">
                <Container maxWidth='md'>
                    <h5> View Opportunity</h5>
                    <Row >
                        <Col sm="12">
                            <FormGroup>
                                <label><b>Opportunity Subject</b></label><br></br>
                                <FormLabel>{Opportunitydata[0].OpportunitySubject}</FormLabel>
                            </FormGroup>
                        </Col>
                        <Col sm="12">
                            <FormGroup>
                                <label><b>Opportunity Description</b></label><br></br>
                                <FormLabel>{Opportunitydata[0].OpportunityDescription}</FormLabel>
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        {StudentId == 2 &&

                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Age Limit Start</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].AgeLimitStart}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 2 &&

                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Age Limit End</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].AgeLimitEnd}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 2 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Status</b></label><br></br>
                                    <FormLabel>{(Opportunitydata[0].Status == 1) ? 'Active' : (Opportunitydata[0].Status == 2) ? 'Expired' : 'Draft'} </FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 3 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Required Position</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].RequiredPosition}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 3 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>District</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].DistrictName}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 3 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Applied</b></label><br></br>
                                    <FormLabel>{length}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                    </Row>

                    <Row >
                        {StudentId == 3 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Approved</b></label><br></br>
                                    <FormLabel>{postion.length}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 2 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Required Position</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].RequiredPosition}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 2 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>District</b></label><br></br>
                                    <FormLabel>{Opportunitydata[0].DistrictName}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                        {StudentId == 2 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Applied</b></label><br></br>
                                    <FormLabel>{length}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                    </Row>
                    <Row>
                        {StudentId == 2 &&
                            <Col sm="4">
                                <FormGroup>
                                    <label><b>Approved</b></label><br></br>
                                    <FormLabel>{postion.length}</FormLabel>
                                </FormGroup>
                            </Col>
                        }
                    </Row>

                    <div className="col text-center">
                        {StudentId == 2 && statusId == 0 &&
                            <Button className="mb-20" variant="primary" onClick={this.updateOpportunity}> Publish </Button>
                        }
                        {StudentId == 2 &&
                            <Button className="mb-20" onClick={this.EditOpportunitiy.bind(this, Opportunitydata[0].id)} size="small" color="primary" justify="right">    Edit </Button>
                        }

                        {(StudentId == 3 && Status == 0 ) &&
                            <Button className="mb-20" onClick={this.applyStudent} size="small" color="primary" justify="right"> Apply </Button>
                        }
                        <Button className="mb-20" variant="secondary" onClick={this.cancelButton} >Cancel</Button>
                    </div>

                </Container>
                {StudentId == 2 &&
                    <Container maxWidth='md'>
                        <div>
                            <MUIDataTable
                                title={"Applied Student List"}
                                data={rows}
                                columns={columns}
                                options={{
                                    selectableRows: false,
                                    print: false,
                                    viewcolumns: false,
                                    download: false,
                                    viewColumns: false,

                                }}
                            />
                        </div>
                    </Container>
                }
            </div >
        )
    }
};


const mapStateToProps = state => ({
    OpportnuityReducer: state.OpportnuityReducer,
    studentReducer: state.studentReducer,
    appliedReducer: state.appliedReducer

});

const mapDispatchToProps = {
    getOpportunityListByid,
    updateOpportunity,
    updateOpportunity,
    updateStudent,
    getAppliedList,
    newApplied
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
