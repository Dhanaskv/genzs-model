import { Card } from "@material-ui/core";
import React, { Component } from "react";
import { Button, Row, Col, FormGroup, FormLabel } from 'react-bootstrap';
import { Field, reduxForm, change, SubmissionError, initialize } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea, MyDropdown } from '../FieldComponent/Field';
import { Container, TextField, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { required, maxLength250 } from '../../helpers/validations'
import { createOpportunity } from '../../Actions/Opportunity';
import { getOrganizationList } from '../../Actions/Organization';
import { getschoolDitstickList } from '../../Actions/schooldistick';



import authGuard from '../../helpers/authGuard'
const AuthService = new authGuard()





class Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: 'none',
            StatusArray: [
                { label: "Draft", value: '0' },
                { label: "Active", value: '1' },
                { label: "Expired", value: '2' },
            ],
            Publish: false
        }
    }

    componentDidMount = async () => {
        await this.props.getOrganizationList()
        await this.props.getschoolDitstickList()
        console.log('this.props.addopportunity ::::::::', this.props)
        this.props.dispatch(initialize(
            'addOpportunity',
            {
            }));
    };

    saveUser = async (value, Publish) => {
        let user_details = AuthService.getProfile();
        let values = value;


        if(this.state.Publish == true){  
            if (user_details.OrganizationId) {
                values.Organization = user_details.OrganizationId
                values.Status =  { label: "Active", value: '1' }
            }
            let response = await this.props.createOpportunity(values)
            if (response.success) {
                this.props.showNotification('success', 'Registration!', `The  Opportunity has been Published successfully.`);
                this.props.history.push('/opportunity')
            }

        }else{
            if (user_details.OrganizationId) {
                values.Organization = user_details.OrganizationId
                values.Status =  { label: "Draft", value: '0' }

            }
            let response = await this.props.createOpportunity(values)
            if (response.success) {
                this.props.showNotification('success', 'Registration!', `The Opportunity  has been saved as Draft.`);
                this.props.history.push('/opportunity')
            }

        }

       
    }

    cancelButton = (e) => {
        this.props.history.push('/opportunity')

    }

    render() {
        const { handleSubmit } = this.props;

        const { schoolDistickReducer } = this.props
        const { schoolDistickList } = schoolDistickReducer;

        let districtArray = [];
        schoolDistickList.forEach(element => {
            districtArray.push({ label: element.DistrictName, value: element.id })
        });

        return (
            <div>
                <form onSubmit={handleSubmit(this.saveUser)}>
                    <Container maxWidth="md">
                        <h5> New Opportunity</h5>
                        <Row >
                            <Col sm="12">
                                <FormGroup>
                                    <FormLabel>Opportunity Subject<span className="astriek"> *</span></FormLabel>
                                    <Field name="OpportunitySubject" component={myTextArea} type="text" style={{ height: '60px' }} readOnly={true} validate={[required, maxLength250]} />
                                </FormGroup>
                            </Col>
                            <Col sm="12">
                                <FormGroup>
                                    <FormLabel>Opportunity Description<span className="astriek"> *</span></FormLabel>
                                    <Field name="OpportunityDescription" component={myTextArea} type="text" style={{ height: '140px' }} validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col sm="4">
                                <FormGroup>
                                    <FormLabel>Age Limit Start<span className="astriek"> *</span></FormLabel>
                                    <Field name="AgeLimitStart" component={myInputNumberOnly} type="text" validate={required} />
                                </FormGroup>
                            </Col>
                            <Col sm="4">
                                <FormGroup>
                                    <FormLabel>Age Limit End<span className="astriek"> *</span></FormLabel>
                                    <Field name="AgeLimitEnd" component={myInputNumberOnly} type="text" validate={required} />
                                </FormGroup>
                            </Col>
                            {/* <Col sm="4">
                                <FormGroup>
                                    <FormLabel>Status<span className="astriek"> *</span></FormLabel>
                                    <Field name="Status" component={MyDropdown} options={this.state.StatusArray} validate={[required]} />
                                </FormGroup>
                            </Col> */}
                            <Col sm="4">
                                <FormGroup>
                                    <FormLabel>District<span className="astriek"> *</span></FormLabel>
                                    <Field name="OpportunityDistrict" component={MyDropdown} options={districtArray} type="text" validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row >
                            

                            <Col sm="4">
                                <FormGroup>
                                    <FormLabel>Required Position<span className="astriek"> *</span></FormLabel>
                                    <Field name="RequiredPosition" component={myInputNumberOnly} type="text" validate={required} />
                                </FormGroup>
                            </Col>

                        </Row>

                        <div className="col text-center">
                            <Button className="mb-20" variant="primary" type="submit">
                                Save as Draft
                                 </Button>
                            <Button className="mb-20" variant="primary" type="submit" onClick={ () => this.setState({Publish: true})}> 
                                Publish
                                 </Button>
                            <Button className="mb-20" variant="secondary" onClick={this.cancelButton} >Cancel</Button>

                        </div>
                    </Container>
                </form>
            </div>
        )
    }
};


const createProductForm = reduxForm({ form: 'addOpportunity' })
const AddOpp = createProductForm(Add);

const mapStateToProps = state => ({
    OpportnuityReducer: state.OpportnuityReducer,
    OrganizationReducer: state.OrganizationReducer,
    schoolDistickReducer: state.schoolDistickReducer,
     


});

const mapDispatchToProps = {
    createOpportunity,
    getOrganizationList,
    getschoolDitstickList
};


export default connect(mapStateToProps, mapDispatchToProps)(AddOpp)
