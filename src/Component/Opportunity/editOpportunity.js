import { Card } from "@material-ui/core";
import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Button, Row, Col, FormGroup, FormLabel } from 'react-bootstrap';
import { Field, reduxForm, change, SubmissionError, initialize } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea, MyDropdown } from '../FieldComponent/Field';
import { Container, TextField, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { required } from '../../helpers/validations'
import { updateOpportunity, getOpportunityListByid } from '../../Actions/Opportunity';
import { getOrganizationList } from '../../Actions/Organization';
import { getschoolDitstickList } from '../../Actions/schooldistick';


class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: 'none',
            StatusArray: [
                { label: "Draft", value: '0' },
                { label: "Active", value: '1' },
                { label: "Expired", value: '2' },
            ],
            Id: 0 
        }
    }

    componentDidMount = async () => {
        let id = this.props.location.state.Id
        await this.props.getOpportunityListByid(id)
        await this.props.getOrganizationList()
        await this.props.getschoolDitstickList()
        var District = this.props.schoolDistickReducer.schoolDistickList
        var Opportunity = this.props.OpportnuityReducer.OpportnuityListByid;
        this.props.dispatch(initialize(
            'EditOpportunity',
            {
                OpportunitySubject: Opportunity[0].OpportunitySubject,
                Organization: Opportunity[0].OrganizationId,
                AgeLimitStart: Opportunity[0].AgeLimitStart,
                AgeLimitEnd: Opportunity[0].AgeLimitEnd,
                OpportunityDescription: Opportunity[0].OpportunityDescription,
                Status: { value: Opportunity[0].Status, label: (Opportunity[0].Status == 1) ? "Active" :  (Opportunity[0].Status == 2) ? "Expired" : "Draft" },
                RequiredPosition: Opportunity[0].RequiredPosition,
                OpportunityDistrict: {value:  Opportunity[0].OpportunityDistrict ,label: Opportunity[0].DistrictName}

            }));
            this.setState({Id: Opportunity[0].Id})
    }

    updateOpportunity = async (value) => {
        console.log('value,', value)
        let values = value
        values.Id  = this.state.Id
        
        let response = await this.props.updateOpportunity(values)
        if (response.success) {
            this.props.showNotification('success', 'Registration!', `The Opportunity  has been Updated successfully.`);
            this.props.history.push('/opportunity')
        }
    }

    cancelButton = (e) => {
        this.props.history.push('/opportunity')

    }

    render() {

        const { handleSubmit } = this.props;

        const { schoolDistickReducer } = this.props
        const { schoolDistickList } = schoolDistickReducer;

        let districtArray = [];
        schoolDistickList.forEach(element => {
            districtArray.push({ label: element.DistrictName, value: element.id })
        });
        return (
            <div>
                    <form onSubmit={handleSubmit(this.updateOpportunity)}>
                        <Container maxWidth='md'>
                            <h5> Edit Opportunity</h5>
                            <Row >
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Opportunity Subject<span className="astriek"> *</span></FormLabel>
                                        <Field name="OpportunitySubject" component={myTextArea} type="text" style={{ height: '60px' }} placeholder="OpportunitySubject" readOnly={true} validate={[required]} />
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Opportunity Description<span className="astriek"> *</span></FormLabel>
                                        <Field name="OpportunityDescription" component={myTextArea} type="text" style={{ height: '140px' }} placeholder="Opportunity Description" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Age Limit Start<span className="astriek"> *</span></FormLabel>
                                        <Field name="AgeLimitStart" component={myInput} type="text" placeholder="AgeLimit Start" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Age Limit End<span className="astriek"> *</span></FormLabel>
                                        <Field name="AgeLimitEnd" component={myInput} type="text" placeholder="AgeLimit End" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Status<span className="astriek"> *</span></FormLabel>
                                        <Field name="Status" component={MyDropdown} options={this.state.StatusArray} validate={[required]} />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>District<span className="astriek"> *</span></FormLabel>
                                        <Field name="OpportunityDistrict" component={MyDropdown} options={districtArray} type="text" validate={required} />
                                    </FormGroup>
                                </Col>

                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Required Position<span className="astriek"> *</span></FormLabel>
                                        <Field name="RequiredPosition" component={myInputNumberOnly} type="text" placeholder="RequiredPosition" validate={required} />
                                    </FormGroup>
                                </Col>


                            </Row>
                            <div className="col text-center">
                                <Button className="mb-20" variant="primary" type="submit">
                                    Update
                                 </Button>
                                <Button className="mb-20" variant="secondary" onClick={this.cancelButton} >Cancel</Button>
                            </div>
                        </Container>
                    </form>
            </div>
        )
    }
};


const editProductForm = reduxForm({ form: 'EditOpportunity' })
const editOpp = editProductForm(Edit);

const mapStateToProps = state => ({
    OpportnuityReducer: state.OpportnuityReducer,
    OrganizationReducer: state.OrganizationReducer,
    schoolDistickReducer: state.schoolDistickReducer


});

const mapDispatchToProps = {
    updateOpportunity,
    getOpportunityListByid,
    getOrganizationList,
    getschoolDitstickList
};


export default connect(mapStateToProps, mapDispatchToProps)(editOpp)