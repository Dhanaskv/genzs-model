import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { getFunOrganizationList } from '../../Actions/Organization';
import MaterialDataTable from '../../helpers/reusable/materialTable';
import { Container } from '@material-ui/core';
import moment from 'moment';
import Tooltip from '@material-ui/core/Tooltip';


const applyTableWith = (value, column) => {
    return () => (
        {

            style: {
                'width': value ? value + 'px' : 'auto',
                'paddingLeft': column === 'Action' && '30px'
            }
        }
    )
};

function OrganizationList(props) {
    const dispatch = useDispatch();
    const [tableData, setTableData] = useState([]);
    const OrganizationReducer = useSelector((state) => state.OrganizationReducer);
    const columns = [
        {
            name: 'id',
            label: 'ID',
            options: {
                filter: false,
                sort: false,
                searchable: false,
                display: false
            }
        },
        {
            name: 'Organizationname',
            label: 'Organization Name',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'textField',
                draggable: true,
                customBodyRender: (item, tableMeta) => {
                    return <Tooltip title={`View ${item}`}><a onClick={() => viewNGO(tableMeta.rowData[0])}>{item}</a></Tooltip>;
                },
                setCellHeaderProps: applyTableWith(200, 'Action'),
                setCellProps: applyTableWith(150, 'Action'),
            }
        },
        {
            name: 'ContactPersonFirstName',
            label: 'contact Person Name',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'textField',
                draggable: true,
                setCellHeaderProps: applyTableWith(200),
            }
        },
        {
            name: 'Website',
            label: 'Website',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'textField',
                draggable: true,
                setCellHeaderProps: applyTableWith(200),
            }
        },
        {
            name: 'ContactPersonMobileNumber',
            label: 'Contact Number',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'textField',
                draggable: true,
                setCellHeaderProps: applyTableWith(200),
            }
        },
        {
            name: 'Status',
            label: 'Status',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'dropdown',
                draggable: true,
                setCellHeaderProps: applyTableWith(100),
            }
        },
        {
            name: 'CreatedOn',
            label: 'Created Date',
            options: {
                filter: true,
                sort: true,
                searchable: true,
                filterType: 'dropdown',
                draggable: true,
                setCellHeaderProps: applyTableWith(150),
            }
        },


    ]
    // const GetNewGuid = () => {
    //     const newGuid = () => {
    //         return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    //             var r = (Math.random() * 16) | 0,
    //                 v = c == 'x' ? r : (r & 0x3) | 0x8;
    //             return v.toString(16);
    //         });
    //     };

    //     return newGuid();
    // };
    const defaultApi = async () => {
        await getFunOrganizationList(dispatch);
    }
    useEffect(() => {
        defaultApi();
    }, []);

    useEffect(() => {
        if (OrganizationReducer.OrganizationList && OrganizationReducer.OrganizationList.length > 0) {
            OrganizationReducer.OrganizationList.map((data) => {
                data.CreatedOn = moment(data.CreatedOn).format('YYYY-MM-DD')
            })
            setTableData(OrganizationReducer.OrganizationList)
        } else if (OrganizationReducer.OrganizationList.lenght === 0) {
            setTableData(OrganizationReducer.OrganizationList)
        }
    }, [OrganizationReducer.OrganizationList]);
    console.log('tableData', tableData)

    const viewNGO = (id) => {
        props.history.push({
            pathname: '/organization',
            state: {
                id: id
            }
        })
    }

    return (
        <>
            {/* <MuiThemeProvider theme={getMuiTheme()}>
                <MUIDataTable
                    title={""}
                    data={tableData}
                    columns={columns}
                    options={{
                        selectableRows: false,
                        print: false,
                        viewcolumns: false,
                        download: false,
                    }}
                />
            </MuiThemeProvider> */}
            <div id="overide-link" >
                <Container maxWidth='lg'>
                    <MaterialDataTable
                        title="NGO List"
                        data={tableData}
                        columns={columns}
                    />
                </Container>
            </div>

        </>
    )
};

export default React.memo(OrganizationList);