import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getOrganizationById, updatedStatusOrg } from '../../Actions/Organization'
import { Container } from '@material-ui/core';
import { Button, Row, Col, FormGroup, Breadcrumb, Form } from 'react-bootstrap';
import { CustomModal, Action, Content } from '../../helpers/reusable/CustomModal';
import authGuard from '../../helpers/authGuard';

const AuthGuard = new authGuard();

function ViewOrganization(props) {
    const dispatch = useDispatch();
    const [orgId, setOrgId] = useState(props.location.state.id)
    const [loader, setLoader] = useState(false)
    const [orgDetail, setOrgDetail] = useState([]);
    const [Approved, setApproved] = useState(false);
    const [Reject, setReject] = useState(false);
    const [rejectReason, setRejectReason] = useState('');
    const [error, setError] = useState('none')
    const OrganizationReducer = useSelector((state) => state.OrganizationReducer);

    const defaultApi = async () => {
        await getOrganizationById(orgId, dispatch);
    }

    useEffect(() => {
        defaultApi();
    }, []);

    useEffect(() => {
        if (OrganizationReducer.organizationById && OrganizationReducer.organizationById.length > 0) {
            setOrgDetail(OrganizationReducer.organizationById);
            setLoader(true)
        } else {
            setLoader(false)
        }
    }, [OrganizationReducer.organizationById]);

    const updateOrganization = async (type) => {
        console.log('approved', type, orgId);
        let user = AuthGuard.getProfile();
        if (type === 'Aprroved') {
            let value = {
                Status: 2,
                id: orgId,
                UpdatedBy: user.id,
                registerEmail: orgDetail[0].ContactPersonEmail,
                userName: `${orgDetail[0].ContactPersonFirstName} ${orgDetail[0].ContactPersonLastName}`
            }
            let Response = await updatedStatusOrg(value, dispatch);
            if (Response.success) {
                props.showNotification('success', 'Status!', `Updated Sucessfully`);
                setApproved(false)
                props.history.push('/organizations');
            }
        } else {
            if (rejectReason && rejectReason != '') {
                let value = {
                    Status: 3,
                    id: orgId,
                    UpdatedBy: user.id,
                    RejectedReason: rejectReason,
                    registerEmail: orgDetail[0].ContactPersonEmail
                }
                console.log('email', value, orgDetail[0].ContactPersonEmail)
                let Response = await updatedStatusOrg(value, dispatch);
                if (Response.success) {
                    props.showNotification('success', 'Status!', `Updated Sucessfully`);
                    setReject(false)
                    props.history.push('/organizations');
                }
            } else {
                setError('block')
            }
        }

    }

    console.log('coming in viewOrganization', rejectReason);
    return (
        <>
            { loader && <div className="container-form">
                {/* <label className="breadcrumb">NGO List/</label>
                <label>View Organization</label> */}
                <Container maxWidth='lg'>
                    <Breadcrumb className="breadcrumbs-cs">
                        <Breadcrumb.Item href="/organizations">NGO List</Breadcrumb.Item>
                        <Breadcrumb.Item active>View NGO</Breadcrumb.Item>
                    </Breadcrumb>
                </Container>
                <Container maxWidth='md'>
                    <h3>Organization Details</h3>
                    <Row>
                        <Col md={'4'}>
                            <label><b>Organization Name</b></label>
                            <FormGroup>{orgDetail[0].Organizationname}</FormGroup>
                        </Col>
                        <Col md={'4'}>
                            <label><b>Website</b></label>
                            <FormGroup>{orgDetail[0].Website}</FormGroup>
                        </Col>
                        <Col md={'4'}>
                            <label><b>Official Email</b></label>
                            <FormGroup>{orgDetail[0].OfficialEmail}</FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <label><b>Short Summary</b></label>
                            <FormGroup>{orgDetail[0].ShortSummary}</FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <label><b>About us </b></label>
                            <FormGroup>{orgDetail[0].AboutUs}</FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <label><b>Address</b></label>
                            <FormGroup>{orgDetail[0].Address}</FormGroup>
                        </Col>
                    </Row>
                    <h3>Contact Person Details</h3>
                    <Row>
                        <Col md={'4'}>
                            <label><b>Name</b></label>
                            <FormGroup>{orgDetail[0].ContactPersonFirstName}</FormGroup>
                        </Col>
                        <Col md={'4'}>
                            <label><b>Role</b></label>
                            <FormGroup>{orgDetail[0].Organizationname}</FormGroup>
                        </Col>
                        <Col md={'4'}>
                            <label><b>Email</b></label>
                            <FormGroup>{orgDetail[0].ContactPersonEmail}</FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={'4'}>
                            <label><b>Contact Number</b></label>
                            <FormGroup>{`${orgDetail[0].ContactPersonFirstName} ${orgDetail[0].ContactPersonLastName}`}</FormGroup>
                        </Col>
                        <Col md={'4'}>
                            <label><b>Status</b></label>
                            <FormGroup>{orgDetail[0].Status}</FormGroup>
                        </Col>
                    </Row>
                    {orgDetail[0].Status == 'Rejected' && <Row>
                        <Col md>
                            <label><b>Rejected Reason</b></label>
                            <FormGroup>{orgDetail[0].RejectedReason}</FormGroup>
                        </Col>
                    </Row>} 
                </Container>
                <div className="col text-center">
                    {orgDetail[0].Status == 'Requested' && <> <Button className="mb-20" variant="primary" onClick={() => setApproved(true)} >Approve </Button> {' '}
                        <Button className="mb-20" variant="danger" onClick={() => setReject(true)}> Reject </Button> </>}

                </div>
            </div>
            }
            <div>
                <CustomModal open={Approved}
                    onClick={() => setApproved(false)}
                    title={'Are you sure to approve? Click yes'} >
                    <Action><Button variant="primary" onClick={() => updateOrganization('Aprroved')}>YES</Button>{'  '}<Button onClick={() => setApproved(false)} variant="secondary">NO</Button></Action>
                </CustomModal>
            </div>
            <div>
                <CustomModal open={Reject}
                    onClick={() => setReject(false)}
                    title={'Please enter reason for Rejection. Click Submit and Cancel'}
                    maxWidth={"md"} >
                    <Content>
                        <Form.Control as="textarea" placeholder={'Reasons for rejection'} onChange={(e) => setRejectReason(e.target.value)} rows={3} />
                        <div className="error_login" style={{ display: error }}>Reason for rejection is required</div>
                    </Content>
                    <Action><Button variant="primary" onClick={() => updateOrganization('Reject')}>Submit</Button>{'  '}<Button onClick={() => setReject(false)} variant="secondary">Cancel</Button></Action>
                </CustomModal>
            </div>

        </>
    )

};


export default ViewOrganization;