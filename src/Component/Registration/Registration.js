import React, { Component } from "react";
import { Button, Row, Col, FormGroup, FormLabel } from 'react-bootstrap';
import { Field, reduxForm, SubmissionError, initialize } from 'redux-form';
import { myInput, myInputNumberOnly, myTextArea } from '../FieldComponent/Field';
import { Container } from '@material-ui/core';
import { connect } from 'react-redux';
import { getStudentDetail, createNewStudent } from '../../Actions/student';
import { emailAlreadyExits } from '../../Actions/user';
import { createNewOrganization } from '../../Actions/Organization'
import { required, email, minValue8, normalizePhone, isDOBValid1, maxLength250 } from '../../helpers/validations';
import moment from 'moment';

const CurrentDate = moment().format('YYYY-MM-DD');

class RegistrationUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: 'none',
            studentEmailValid: 'none'
        }
    };



    componentDidMount = async () => {
        await this.props.getStudentDetail();
        this.props.dispatch(initialize(
            'newUser'
        ));
        console.log('this.props', this.props)

    };
    saveUser = async (value) => {
        // if (this.props.isStudent == false) {
        //     this.props.history.push('/Editopportunitiy')0
        // }
        let emailAlreadyExit = await this.props.emailAlreadyExits(value);

        if (emailAlreadyExit.emailExist) {
            throw new SubmissionError({
                Email: 'The Mail ID has already been used'
            });
        }

        if (!emailAlreadyExit.emailExist) {
            if (this.props.isStudent) {

                let email = value.Email.toString();
                let domainName = email.slice(email.indexOf("@") + 1, email.length);
                value.DomainName = domainName

                let response = await this.props.createNewStudent(value)
                if (response.success) {
                    this.props.showNotification('success', 'Registration!', `The ${value.FirstName + ' ' + value.LastName} has been Registrated successfully.`);
                    this.props.handleClose();
                } else {
                    if (response.emailNValid) {
                        throw new SubmissionError({
                            Email: 'Please Enter your valid School Mail ID'
                        });
                    }
                }
            } else {
                let response = await this.props.createNewOrganization(value);
                if (response.success) {
                    this.props.showNotification('success', 'Registration!', `Thank you for registering with us. You will get a confirmation email to access the system.`);
                    this.props.handleClose();
                };
            };
        }
    }

    render() {
        const { handleSubmit } = this.props;
        const { studentEmailValid } = this.state;
        return (
            <div>
                {/* <Card className=''> */}
                <form onSubmit={handleSubmit(this.saveUser)}>
                    {this.props.isStudent ? <Container className='container-form' maxWidth="md" >
                        <Row >
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>First Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="FirstName" component={myInput} type="text" placeholder="Enter First Name" validate={required} />
                                </FormGroup>
                            </Col>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>Last Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="LastName" component={myInput} type="text" placeholder="Enter Last Name" validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>Email<span className="astriek"> *</span></FormLabel>
                                    <Field name="Email" component={myInput} type="email" placeholder="Enter Email" validate={required, email} />
                                </FormGroup>
                            </Col>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>Password<span className="astriek"> *</span></FormLabel>
                                    <Field name="Password" component={myInput} type="password" placeholder="Enter Password" validate={required, minValue8} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>Grade<span className="astriek"> *</span></FormLabel>
                                    <Field name="Grade" component={myInput} type="text" placeholder="Enter Your Grade" validate={required} />
                                </FormGroup>
                            </Col>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>School Name<span className="astriek"> *</span></FormLabel>
                                    <Field name="SchoolName" component={myInput} type="text" placeholder="Enter School Name" validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs sm="6">
                                <FormGroup>
                                    <FormLabel>DOB<span className="astriek"> *</span></FormLabel>
                                    <Field name="DOB" component={myInput} type="date" max={CurrentDate} placeholder="Enter DOB" validate={required, isDOBValid1} />
                                </FormGroup>
                            </Col>
                            <Col sm="6">
                                <FormGroup>
                                    <FormLabel>Phone No<span className="astriek"> *</span></FormLabel>
                                    <Field name="PhoneNo" component={myInputNumberOnly} normalize={normalizePhone} type="text" placeholder="Enter Phone No" validate={required} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <div className="col text-center">
                            <Button className="mb-20" variant="primary" type="submit">
                                Submit
                                 </Button>
                        </div>
                    </Container> :
                        <Container className='container-form' maxWidth='md'>
                            <h5>Organization Details</h5>
                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Organization name<span className="astriek"> *</span></FormLabel>
                                        <Field name="OrganizationName" component={myInput} type="text" placeholder="Enter Organization Name" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Website<span className="astriek"> *</span></FormLabel>
                                        <Field name="Website" component={myInput} type="text" placeholder="Enter Website" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Official Email<span className="astriek"> *</span></FormLabel>
                                        <Field name="OfficialEmail" component={myInput} type="text" placeholder="Enter Offical Email" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row >
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Short Summary<span className="astriek"> *</span></FormLabel>
                                        <Field name="ShortSummary" component={myTextArea} rows={1} type="text" placeholder="Short Summary..." validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>About us<span className="astriek"> *</span></FormLabel>
                                        <Field name="AboutUs" component={myTextArea} rows={1} type="text" placeholder="Enter About the Organization" validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <FormLabel>Address<span className="astriek"> *</span></FormLabel>
                                        <Field name="Address" component={myTextArea} rows={2} type="text" placeholder="Enter Address" validate={required, maxLength250} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <h5>Contact Person Details</h5>
                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>First Name<span className="astriek"> *</span></FormLabel>
                                        <Field name="FirstName" component={myInput} type="text" placeholder="Enter First Name" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Last Name<span className="astriek"> *</span></FormLabel>
                                        <Field name="LastName" component={myInput} type="text" placeholder="Enter Last Name" validate={required} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Role<span className="astriek"> *</span></FormLabel>
                                        <Field name="Role" component={myInput} type="text" placeholder="Enter Role" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row >
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Email<span className="astriek"> *</span></FormLabel>
                                        <Field name="Email" component={myInput} type="text" placeholder="Enter Email" validate={required, email} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Password<span className="astriek"> *</span></FormLabel>
                                        <Field name="Password" component={myInput} type="password" placeholder="Enter  Password" validate={required, minValue8} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <FormLabel>Contact Number<span className="astriek"> *</span></FormLabel>
                                        <Field name="PhoneNo" component={myInputNumberOnly} normalize={normalizePhone} type="text" placeholder="Enter Contact Number" validate={required} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <div className="col text-center">
                                <Button className="mb-20" variant="primary" type="submit">
                                    Submit
                                 </Button>
                            </div>
                        </Container>
                    }
                </form>
                {/* </Card> */}
            </div>
        )
    }
};

const createProductForm = reduxForm({ form: 'newUser' })

const Registration = createProductForm(RegistrationUser);

const mapStateToProps = state => ({
    studentReducer: state.studentReducer
});

const mapDispatchToProps = {
    getStudentDetail,
    createNewStudent,
    createNewOrganization,
    emailAlreadyExits
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
