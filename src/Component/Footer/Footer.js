import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //  
        };
    };

    render() {
        return (
            <footer className="footer disable-print">

                <div className="col-lg-12 col-12">
                    <p className="copyright" style={{ textAlign: 'center' }}>Copyright &copy; 2021 GenZS . All rights reserved</p>
                </div>

            </footer>
        )
    }
};

export default Footer;