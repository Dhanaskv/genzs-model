import React, { Component, useState } from 'react';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import { Grid, Paper, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";


export function HomeComponent(props) {
    return (
        <div>
            {/* About */}
            <section className="about full-screen d-lg-flex justify-content-center align-items-center" id="about">
                <div className="container">
                    <div className="row">

                        <div className="col-lg-7 col-md-12 col-12 d-flex align-items-center">
                            <div className="about-text">
                                <small className="small-text">Welcome to <span className="mobile-block">GENZS!</span></small>
                                <h1 className="animated animated-text">
                                    <span className="mr-2">Are you </span>
                                    <div className="animated-info">
                                        <span className="animated-item">a Student</span>
                                        <span className="animated-item">an Organization</span>
                                    </div>
                                </h1>

                                <p className="motto">Strengthening Unity in Our Communities</p>

                                <div className="custom-btn-group mt-4">
                                    <a href="#" className="btn mr-lg-2 custom-btn"><i className='uil uil-file-alt'></i> I'm a Student</a>
                                    <a href="#contact" className="btn custom-btn custom-btn-bg custom-btn-link">I'm Looking for Help</a>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-5 col-md-12 col-12">
                            <div className="about-image svg">
                                <img src={require("../assets/images/banner.svg")} className="img-fluid" alt="svg image" />
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            {/* About Us */}
            <section className="aboutus py-5" id="aboutus">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 col-12">
                            <h2 className="mb-4">About GenZS</h2>
                            <p>We are a Non-Profit organization focused to provide the community services for the elderly homes, service organizations and needy people in the neighborhood with the teams of highly energetic and social minded High School students. During this tougher times of COVID-19, needy people and organizations are having difficulty to find volunteers and my fellow students are finding difficulties in finding fulfilling and school required volunteer opportunities. We are committed to bridge this gap and make fruitful and fulfilling experiences to both students and community organizations. Let us step up and work together to make the world a better place.</p>
                        </div>

                        <div className="col-lg-5 col-12">

                            <img src={require("../assets/images/logo.svg")} className="img-fluid" alt="svg image" />
                        </div>
                    </div>
                </div>
            </section>

            {/* FEATURES */}

            <section className="services py-5 d-lg-flex justify-content-center align-items-center" id="services">
                <div className="container">
                    <div className="row">

                        <div className="col-lg-6 col-12">
                            <h2 className="mb-4">Student</h2>

                            <div className="timeline">
                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>1</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Singup</span><small></small></h3>
                                        <p>High school students will sign up by submitting their grade and school email so they can have easy access to submitting required volunteer hours. Non-students can sign up in a more conventional method with a regular email to save their data.</p>
                                    </div>
                                </div>

                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>2</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Search Opportunity</span><small></small></h3>
                                        <p>Once you sign up, you can now search for volunteering opportunties that are nearby and match with your interests.</p>
                                    </div>
                                </div>

                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>3</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Choose Opportunity</span></h3>
                                        <p>Volunteers can click on the venture they want to lend a hand to.</p>
                                    </div>
                                </div>

                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>4</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Start Volunteering<small></small></span></h3>
                                        <p>Students will be able to start volunteering at their desired locations and help strengthen their community.</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="col-lg-6 col-12">
                            <h2 className="mb-4 mobile-mt-2">Orgnaization</h2>

                            <div className="timeline">
                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>1</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Singup</span><small></small></h3>
                                        <p>If you are looking for volunteers, you will redirected to a sign-up page where you have to go through a verification process. </p>
                                    </div>
                                </div>

                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>2</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Post Opportunity</span><small></small></h3>
                                        <p>Organizations, after completing the verification process, can post their opportunity and specify how many volunteers are needed and how many hours it will take.</p>
                                    </div>
                                </div>

                                <div className="timeline-wrapper">
                                    <div className="timeline-yr">
                                        <span>3</span>
                                    </div>
                                    <div className="timeline-info">
                                        <h3><span>Manage Opportunity</span><small></small></h3>
                                        <p>Organizations will also be given to opportunity to manage their posts by being able to edit, change timings and dates, and close opportunities at will.</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </section>

            {/* CONTACT */}
            <section className="contact py-5" id="contact">
                <div className="container">
                    <div className="row">

                        <div className="col-lg-5 mr-lg-5 col-12">
                            <div className="google-map w-100">

                            </div>

                            <div className="contact-info d-flex justify-content-between align-items-center py-4 px-lg-5">
                                <div className="contact-info-item">
                                    <h3 className="mb-3 text-white">Say hello</h3>
                                    <p className="footer-text mb-0">(425) 283-7686</p>
                                    <p><a href="mailto:dhruvs@genzs.org">dhruvs@genzs.org</a></p>
                                </div>

                                <ul className="social-links">
                                    <li><a href="#" className="uil uil-dribbble" data-toggle="tooltip" data-placement="left" title="Dribbble"></a></li>
                                    <li><a href="#" className="uil uil-instagram" data-toggle="tooltip" data-placement="left" title="Instagram"></a></li>
                                    <li><a href="#" className="uil uil-youtube" data-toggle="tooltip" data-placement="left" title="Youtube"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-lg-6 col-12">
                            <div className="contact-form">
                                <h2 className="mb-4">Interested to work together? Let's talk</h2>

                                <form action="" method="get">
                                    <div className="row">
                                        <div className="col-lg-6 col-12">
                                            <input type="text" className="form-control" name="name" placeholder="Your Name" id="name" />
                                        </div>

                                        <div className="col-lg-6 col-12">
                                            <input type="email" className="form-control" name="email" placeholder="Email" id="email" />
                                        </div>

                                        <div className="col-12">
                                            <textarea name="message" rows="6" className="form-control" id="message" placeholder="Message"></textarea>
                                        </div>

                                        <div className="ml-lg-auto col-lg-5 col-12">
                                            <input type="submit" className="form-control submit-btn" value="Send Button" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            {/* <section className="contact py-5" id="List">
                <div>
                    <div style={{ marginLeft: 100, padding: 30 }}>
                    <Button style={{marginLeft: '1000px', backgroundColor: '#00BFFF',width: '90px', height: '40px', borderRadius: '5px', color: 'white'}}>Add</Button>
                        <Grid container spacing={70} justify="left" >

                            {posts.map(post => (
                                <Grid item key={post.title} color='primary' width='600px' style={{ marginTop: 10, padding: 10 }}  >
                                     
                                    <div style={{ borderLeft: '6px solid #00BFFF',height: 'auto' }} key={post}>
                                    <Card>
                                        <CardActionArea>
                                            <CardMedia
                                                alt="Contemplative Reptile"
                                                height="100"
                                                title="Contemplative Reptile"

                                            />
                                            <CardContent >

                                                <Typography gutterBottom variant="h5" component="h2" color='black' >
                                                    <b>{post.title}</b>
                                                </Typography>
                                                <Typography gutterBottom variant="h6" component="h3" color='black'>
                                                    <b> {post.job}</b>
                                                </Typography>
                                                <hr />
                                                <Typography component="p">{post.excerpt}</Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button size="small" color="primary" justify="right">
                                                More
                                              </Button>
                                        </CardActions>
                                    </Card>
                                    </div>
                                   
                                </Grid>
                                
                            ))}
                        </Grid>
                </div>
                </div>
            </section> */}

        </div >
    )
}