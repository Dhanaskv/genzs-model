import React, { Component } from "react";
import { ControlLabel, FormControl } from "react-bootstrap";
import Select from 'react-select';

export const myInput = ({ input, type, AddonPrefix, placeholder, maxlength, errormessage, readonly = false, className = false, max, meta: { touched, error, warning }, ...props }) => {

  if (type == "hidden") {
    return (
      <div className="hide">
        <FormControl {...input} type={type}
          disabled={readonly}
          min={type == 'number' ? 0 : ''}
          max={type == 'date' ? max : ''}
          placeholder={placeholder}
        />

        {touched &&
          ((error && <span style={{ color: 'red' }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  } else {
    return (
      <div className={className ? className + ' prefixAddon-input' : 'prefixAddon-input'}>
        {AddonPrefix && <span className='prefixAddon'>{AddonPrefix}</span>}
        <FormControl {...input} type={type}
          min={type == 'number' ? 0 : ''}
          max={type == 'date' ? max : ''}
          maxLength={(maxlength) ? maxlength : ''}
          className={AddonPrefix ? 'prefixAddon-input' : 'form-control'}
          disabled={readonly}
          placeholder={placeholder}
        />
        {touched &&
          ((error && <span style={{ color: 'red' }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  }

};

/**
* <h5>Summary: </h5>
* In this method we render input field based upon the type    
* <ol>
*      <li> Render the field based on the type passed</li>
*      <li> Validate the field using redux form</li>
*      <li> Based on the validation result return the appropriate error message</li>
* </ol>
* */

export const myInputNumberOnly = ({ input, type, placeholder, maxlength, errormessage, readonly = false, optional = false, className = false, meta: { touched, error, submitFailed, warning } }) => {
  if (type == "hidden") {
    return (
      <div className="hide">
        <FormControl {...input} type={type}
          disabled={readonly}
          placeholder={placeholder}
        />

        {touched &&
          ((error && <span style={{ color: 'red' }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  } else {
    return (
      <div className={className ? className : undefined}>
        <FormControl {...input} type={type}
          disabled={readonly}
          placeholder={placeholder}
          maxLength={(maxlength) ? maxlength : ''}
          onKeyPress={event => {
            if (!(event.charCode >= 48 && event.charCode <= 57)) {
              event.preventDefault();
            }
          }}
        />

        {touched && submitFailed && !optional &&
          ((error && <span style={{ color: 'red' }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  }

};

/**
* <h5>Summary: </h5>
* In this method we render text area field   
* <ol>
*      <li> Validate the field using redux form</li>
*      <li> Based on the validation result return the appropriate error message</li>
* </ol>
**/

export const myTextArea = ({ input, type, style, placeholder, rows, meta: { touched, error, warning } }) => {
  return (
    <div>
      <textarea className="form-control" {...input} rows={rows ? rows : 3} style={style} placeholder={placeholder} ></textarea>
      {touched &&
        ((error && <span style={{ color: 'red' }}>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>

  );
};

export const MyDropdown = ({ input, type, placeolder, clsName, isMulti, readonly, className = false, defaultValue = {}, meta: { touched, error, warning }, ...props }) => {
  const handleBlur = () => input.onBlur


  return (
    <div className={clsName}>

      <Select
        instanceId={input.name}
        {...input}
        {...props}
        onBlur={handleBlur}
        isMulti={(isMulti == "true") ? true : false}
        isDisabled={(readonly) ? true : false}

      />
      {touched &&
        ((error && <span style={{ color: 'red', position: 'absolute' }}>{error}</span>) ||
          (warning && <span>{warning}</span>))}

    </div>
  )


}