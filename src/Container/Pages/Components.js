import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom'
import Header from '../../Component/Header/Header';
import Footer from '../../Component/Footer/Footer';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';

class Components extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Components: [
                {
                    name: 'Alert',
                    link: '/badgealert',
                }
            ]
        }
        console.log('dashboard', this.props)
    }


    componentDidMount() { }

    componentDidCatch(error, info) {
        console.log(error, info)
    }

    render() {
        return <div className="">
            <Header {...this.props} />
            <div className="main-content">
                <NotificationContainer />
                {/* <div className="row">
                <div className="col-xs-12 col-md-9 main_board"> */}

                {this.props.children}
                {/* </div>
            </div> */}
            </div>
                <Footer {...this.props} />
        </div>
    }
}

export default Components;