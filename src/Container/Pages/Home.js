import React, { Component, Fragment } from 'react';
import { HomeComponent } from '../../Component/HomeComponent';
import { connect } from 'react-redux';

class HomeContainer extends Component {

    componentDidUpdate(nextProps, nextState) {

    }

    render() {
        const { handleSubmit, submitting } = this.props
        return (
            <Fragment>
                <HomeComponent />
            </Fragment>
        )
    }
}


const mapStateToProps = (state) => ({
    testState: state.testReducer,
})

const mapDispatchToProps = (dispatch) => ({
    testAuth: (values) => dispatch(actioncreators.UserLogin(values)),
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)