import React, { lazy, Suspense, useEffect } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import actioncreators from '../Actions/ActionCreators'
import Components from '../Container/Pages/Components';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import authGuard from '../helpers/authGuard';
import { Redirect } from 'react-router-dom';

const HomeContainer = lazy(() => {
    return import('../Container/Pages/Home');
})
const Register = lazy(() => import('../Component/Registration/Registration'));
// const LogIn = lazy(() => import('../Component/LoginComponent/login'))
const opportunitiy = lazy(() => import('../Component/Opportunity/opportunity'));
const Addopportunitiy = lazy(() => import('../Component/Opportunity/addOpportunity'));
const Editopportunitiy = lazy(() => import('../Component/Opportunity/editOpportunity'));
const Viewopportunitiy = lazy(() => import('../Component/Opportunity/viewOpportunity'));
const Login = lazy(() => import('../Component/LoginComponent/loginnew'));
const NgoList = lazy(() => import('../Component/Organization/organizationList'));
const OrganizationById = lazy(() => import('../Component/Organization/viewOrganization'))
const View = lazy(() => import('../Component/Profile/view'))
const Edit = lazy(() => import('../Component/Profile/edit'))

const showNotification = (type, title, message) => {
    switch (type) {
        case 'info':
            NotificationManager.info(message);
            break;
        case 'success':
            console.log('showNotification', type)
            NotificationManager.success(message, title);
            break;
        case 'warning':
            NotificationManager.warning(message, title, 300000);
            break;
        case 'error':
            NotificationManager.error(message, title, 5000);
            break;
    }
};
const AuthService = new authGuard();
AuthService.validateToken();
let users = AuthService.getProfile();
console.log('coming in the router of the page', users)

const AppRoute = props => {
    useEffect(() => {
        console.log('props its comming', props)
    }, []);
    let _routes = (
        <Components {...props}>
            { /* None login user only can use */
                !users && !!!users ?
                    <Switch>
                        <Route path="/" exact render={() => <HomeContainer />} />
                        <Route path="/new" render={<Register {...props} />} />
                        <Route path="/Login" render={() => <Login {...props} />} />
                        <Route render={() => <Redirect to={{ pathname: "/" }} />} />
                    </Switch> : /* Only Admin Access the Below URL */
                    users && users.RoleId == 1 ?
                        <Switch>
                            <Route path="/organizations" render={() => <NgoList {...props} />} />
                            <Route path="/organization" render={() => <OrganizationById {...props} />} />
                            <Route path="/ViewProfile" render={() => <View {...props} />} />
                            <Route path="/EditProfile" render={() => <Edit {...props} />} />
                            <Route render={() => <Redirect to={{ pathname: "/organizations" }} />} />
                        </Switch> : /* Only NGO Access the Below URL */
                        users && users.RoleId == 2 ?
                            < Switch >
                                <Route path="/opportunity" component={opportunitiy} props={'opportunity'} />
                                <Route path="/Addopportunity" render={() => <Addopportunitiy {...props} />} />
                                <Route path="/Editopportunity" render={() => <Editopportunitiy {...props} />} />
                                <Route path="/Viewopportunity" render={() => <Viewopportunitiy {...props} />} />
                                <Route path="/ViewProfile" render={() => <View {...props} />} />
                                <Route path="/EditProfile" render={() => <Edit {...props} />} />
                                <Route render={() => <Redirect to={{ pathname: "/opportunity" }} />} />
                            </Switch> : /* Only Student Access the Below URL */
                            users && users.RoleId == 3 ?
                                < Switch >
                                    <Route path="/opportunity" component={opportunitiy} props={'opportunity'} />
                                    <Route path="/Viewopportunity" render={() => <Viewopportunitiy {...props} />} />
                                    <Route path="/ViewProfile" render={() => <View {...props} />} />
                                    <Route path="/EditProfile" render={() => <Edit {...props} />} />
                                    <Route render={() => <Redirect to={{ pathname: "/opportunity" }} />} />
                                </Switch>
                                :
                                < Switch >
                                    <Route render={() => <Redirect to={{ pathname: "/" }} />} />
                                </Switch>
            }


        </Components >
    );

    return (
        <div><Suspense fallback={<Components><p>Loading</p></Components>}>{_routes}</Suspense></div>
    );
}



const mapStateToProps = (state) => ({
    testState: state.testReducer,
})

const mapDispatchToProps = (dispatch) => ({
    testAuth: (values) => dispatch(actioncreators.UserLogin(values)),
    showNotification: (type, title, message) => showNotification(type, title, message)
})

const MainRoute = connect(mapStateToProps, mapDispatchToProps)(AppRoute);
export default withRouter(MainRoute);