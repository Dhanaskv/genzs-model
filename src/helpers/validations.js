import axios from './axiosinstance';
import moment from 'moment';

export function getFormattedFieldName(field) {
  let fieldname = field.replace("ID", "");

  fieldname = fieldname.replace(/([A-Z])/g, ' $1').trim()
  return fieldname;
}

export const required = (value, allValues, props, name) => {
  if (value) {
    return undefined;
  } else {

    if (name == 'JSON') {
      return name + " is required";
    }

    let fieldname = getFormattedFieldName(name);
    return fieldname + " is required";
  }
}

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined


const maxLength = max => value =>
  value && value.length > max ? `Value must be less than ${max} character` : undefined

export const maxLength12 = maxLength(12)

export const maxLength250 = maxLength(250)


const minValue = min => value =>
  value && value.length < min ? `password must contain at least 8 characters ${min}` : undefined

export const minValue8 = minValue(8)


/* Phone Number Normalizer */

export const normalizePhone = (value, previousValue) => {
  if (!value) {
    return value
  }
  const onlyNums = value.replace(/[^\d]/g, '')
  if (!previousValue || value.length > previousValue.length) {
    if (onlyNums.length === 3) {
      return '(' + onlyNums + ')'
    }
    if (onlyNums.length === 6) {
      return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3) + ' '
    }
  }
  if (onlyNums.length <= 3) {
    return onlyNums
  }
  if (onlyNums.length <= 6) {
    return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3)
  }
  return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3, 6) + '-' + onlyNums.slice(6, 10)
};

const isDOBValid = year => value =>
  value && value > moment().subtract(`${year}`, 'years').format('YYYY-MM-DD') ? 'Please enter a valid date' : undefined


export const isDOBValid1 = isDOBValid(1)
