import axios from 'axios';

/* 
   * <h5> Summary of fetchClient </h5>
   * <ol> setup base URL </ol>
   *     <li> Base URL Setup </li>
   
*/

const fetchClient = () => {
    const defaultOptions = {
        baseURL: '',
        //  baseURL: 'https://genzs.herokuapp.com/'
        // headers: {
        //     'Content-Type': 'application/json',
        //   },
    };

    let instance = axios.create(defaultOptions);

    return instance;
};

export default fetchClient();

