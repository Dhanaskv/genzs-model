
import React, { Component } from 'react';

import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
// import { Dialog } from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogTitle from '@material-ui/core/DialogTitle';
/**
     * <h5>Summary of CustomModal: </h5>
     * This models is to display the given information in model (Popup) 
     * <ol>
     *      <li> Open, Close , Class Name (ui class name) will be sent as pros form parent component   </li>
     *      <li> "props.title" will be passed form parent component and title will be displayed </li>
     *      <li> "props.children" will receive child props from parent component. "props.children" will display "ModelDialogTitle" , "Action" or all details placed inside "<CustomModal></CustomModal>" tag  </li>
     * </ol>
     * <h6>Last Modified: <small>Akash</small> </h6>
     * <h6>Last updated by: <small>30/09/2019</small> </h6>
     * <h6>Reason for modify: <small>To create doc for Custom Model </small> </h6>
*/
export const CustomModal = (props) => {

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="alert-dialog-title"
            disableBackdropClick={props.disableBackdropClick ? props.disableBackdropClick : false}
            aria-describedby="alert-dialog-description"
            className={props.className ? props.className : ''}
            maxWidth={props.maxWidth ? props.maxWidth : false}
            fullWidth={props.maxWidth ? true : false}
            disableRestoreFocus={props.disableRestoreFocus ? props.disableRestoreFocus : false}
        >
            {
                props.title &&
                <DialogTitle id="alert-dialog-title" >{props.title}</DialogTitle>
            }
            {props.children}
        </Dialog>
    )
}

/**
     * <h5>Summary of Content : </h5>
     * This models is to display the given information in model (Popup) 
     * <ol>
     *      <li> "props.style" (ui styles) will be sent as pros form parent component   </li>
     *      <li> "props.children" will receive child props from parent component. "props.children" will display "ModelDialogTitle" , "Action" or all details placed inside "<CustomModal></CustomModal>" tag  </li>
     * </ol>
     * <h6>Last Modified: <small>Akash</small> </h6>
     * <h6>Last updated by: <small>30/09/2019</small> </h6>
     * <h6>Reason for modify: <small>To create doc for Custom Model </small> </h6>
*/
export const Content = (props) => {
    return (
        <DialogContent style={props.style} className={props.className ? props.className : ''}>
            {props.children}
        </DialogContent>
    )
}

/**
     * <h5>Summary of Action : </h5>
     * This models is to display the given information in model (Popup) 
     * <ol>
     *      <li> "props.children" will receive child props from parent component. "props.children" will display action button (submit, save, close etc) from parent component </li>
     * </ol>
     * <h6>Last Modified: <small>Akash</small> </h6>
     * <h6>Last updated by: <small>30/09/2019</small> </h6>
     * <h6>Reason for modify: <small>To create doc for Custom Model </small> </h6>
*/
export const Action = (props) => {

    return (
        <DialogActions className={props.className ? props.className : "custom-modal-footer"}>
            {props.children}
        </DialogActions>
    )
}
