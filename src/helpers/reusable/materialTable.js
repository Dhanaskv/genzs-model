import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import MUIDataTable from 'mui-datatables';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'


function MaterialDataTable(props) {

    const {
        title,
        columns,
        data,
    } = props
    console.log('props in materialDataTable', props);

    const options = {
        viewColumns: false,
        print: false,
        download: false,
        selectableRows: false
    }
    const getMuiTheme = () =>
        createMuiTheme({
            overrides: {
                // MUIDataTable: {
                //     root: {
                //         backgroundColor: '#c0daed !important',
                //     },
                //     paper: {
                //         boxShadow: 'inherit',
                //     },
                // },
                // MuiToolbar: {
                //     root: {
                //         backgroundColor: '#c0daed',
                //     },
                // },
                MuiTableCell: {
                    head: {
                        backgroundColor: '#c5c6c7 !important',
                    },
                },
                
                // MUIDataTableSelectCell: {
                //     headerCell: {
                //         backgroundColor: 'blue !important',
                //     },
                // },
                // MuiTableFooter: {
                //     root: {
                //         '& .MuiToolbar-root': {
                //             backgroundColor: 'white !important',
                //         },
                //     },
                // },
            },
        });

    return (
        <>
            <MuiThemeProvider theme={getMuiTheme()} >
                <MUIDataTable
                    title={title}
                    data={data}
                    columns={columns}
                    options={options}
                />
            </MuiThemeProvider>
        </>
    )
};

export default React.memo(MaterialDataTable);