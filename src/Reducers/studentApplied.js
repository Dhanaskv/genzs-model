import { STUDENTAPPLIED_LIST } from '../Actions/Actions';

const initialstate = {
    allAppliedlist : []
};

const appliedReducer = (state = initialstate, action) => {
    switch (action.type) {
        case STUDENTAPPLIED_LIST:
            return { ...state, allAppliedlist: [...action.Payload] }

        default:
            return state;
    }
};

export default appliedReducer;