import {SCHOOLDISTICK_LIST} from '../Actions/Actions'


const initialstate = {  schoolDistickList : [] };

const schoolDistickReducer = (state = initialstate, action) => {
    switch (action.type) {
        case SCHOOLDISTICK_LIST:
            return { ...state, schoolDistickList: [...action.Payload] }
        default:
            return state;
    }
};

export default schoolDistickReducer;