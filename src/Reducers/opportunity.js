import {OPPORTUNITY_LIST, OPPORTUNITY_LIST_BYID} from '../Actions/Actions'

const initialstate = {  OpportnuityList : [], OpportnuityListByid : []};

const OpportnuityReducer = (state = initialstate, action) => {
    switch (action.type) {
        case OPPORTUNITY_LIST:
            return { ...state, OpportnuityList: [...action.Payload] }
            case OPPORTUNITY_LIST_BYID:
            return {...state,OpportnuityListByid:[...action.Payload]}
        default:
            return state;
    }
};

export default OpportnuityReducer;