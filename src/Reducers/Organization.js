import { ORGANIZATION_LIST, ORGANIZATION_BY_ID,ORGANIZATION_BY_ID_VIEW } from '../Actions/Actions'


const initialstate = { OrganizationList: [], organizationById: [], organizationByIdView: [] };

const OrganizationReducer = (state = initialstate, action) => {
    switch (action.type) {
        case ORGANIZATION_LIST:
            return { ...state, OrganizationList: [...action.Payload] }
        case ORGANIZATION_BY_ID:
            return { ...state, organizationById: [...action.Payload] }
            case ORGANIZATION_BY_ID_VIEW:
            return { ...state, organizationByIdView: [...action.Payload] }
        default:
            return state;
    }
};

export default OrganizationReducer;

