import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import TestReducer from './TestReducer';
import StudentReducer from './student';
import OpportnuityReducer from './opportunity'
import OrganizationReducer from './Organization'
import schoolDistickReducer from './schooldistick'
import authReducer from './user'
import appliedReducer from './studentApplied'
import adminReducer from './admin'

let RootReducer = combineReducers({
    testReducer: TestReducer,
    form: formReducer,
    studentReducer: StudentReducer,
    OpportnuityReducer: OpportnuityReducer,
    OrganizationReducer: OrganizationReducer,
    schoolDistickReducer: schoolDistickReducer,
    authReducer: authReducer,
    appliedReducer: appliedReducer,
    adminReducer: adminReducer
});

export default RootReducer;