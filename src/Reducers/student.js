import { GET_STUDENT_DETAIL } from '../Actions/Actions';

const initialstate = {
    allStudentDetail : []
};

const studentReducer = (state = initialstate, action) => {
    switch (action.type) {
        case GET_STUDENT_DETAIL:
            return { ...state, allStudentDetail: [...action.Payload] }

        default:
            return state;
    }
};

export default studentReducer;