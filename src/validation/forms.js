export const FormValidation = values => {

    console.log('values', values)

    const errors = {};
    if (!values.textfield) {
        errors.textfield = 'textfield is required'
    }
    if (!values.numberfield) {
        errors.numberfield = 'numberfield is required'
    }
    if (!values.textareafield) {
        errors.textareafield = 'textarea is required'
    }
    if (!values.datefield) {
        errors.datefield = 'datefield is required'
    }
    if (!values.select) {
        errors.select = 'Select is required'
    }
    if (!values.multiselect) {
        errors.multiselect = 'Multi Select is required'
    }
    if (!values.emailfield) {
        errors.emailfield = 'Email is required'
    }
    if (values.emailfield) {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if (!pattern.test(values.emailfield)) {
            errors.emailfield = 'Enter valid email address'
        }
    }
    if (!values.passwordfield) {
        errors.passwordfield = 'Password is required'
    }
    if (values.passwordfield) {
        var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if (!regularExpression.test(values.passwordfield)) {
            errors.passwordfield = 'Password field has atleast and number, one special character and length 6 to 16 character'
        }
    }
    if (!values.inputgroupfield) {
        errors.inputgroupfield = 'Input group field is required'
    }

    //Rejister Form
    if (!values.FirstName) {
        errors.FirstName = 'First Name is required'
    }
    if (!values.Register) {
        errors.Register = 'Register Number is required'
    }

    if (!values.phoneNumber && !/^(0|[1-9][0-9]{9})$/i.test(values.phoneNumber)) {
        errors.phoneNumber = 'Phone number is required'
    }

    if (values.phoneNumber && !/^(0|[1-9][0-9]{9})$/i.test(values.phoneNumber)) {
        errors.phoneNumber = 'Invalid phone number, must be 10 digits'
    }
    if (!values.VendorEffectiveDate) {
        errors.VendorEffectiveDate = 'Date is required'
    }


    return errors
}


function getFormattedFieldName(field) {
    let fieldname = field.replace("ID", "");
    if (fieldname == 'DealerType') {
        fieldname = 'Type'
    }
    if (fieldname == 'ItemCode') {
        fieldname = 'Part Number'
    }
    fieldname = fieldname.replace(/([A-Z])/g, ' $1').trim()
    return fieldname;
}

export const Fieldrequired = (value, allValues, props, name) => {
    if (value) {
        return undefined;
    } else {

        if (name == 'JSON') {
            return "Value is required";
        }
        return "Value is required";
    }
}

export const DynamicFormValidation = values => {
    console.log("valuesvaluesvaluesvalues")
    console.log(values)
    const errors = {}
    if (!values.UserName) {
        errors.UserName = 'UserName is required'
    }
    if (!values.MobileNumber) {
        errors.MobileNumber = 'MobileNumber is required'
    }
    if (!values.selectbox) {
        errors.selectbox = 'selectbox is required'
    }
    if (!values.selectboxmulti) {
        errors.selectboxmulti = 'multi selectbox is required'
    }
    if (!values.singledate) {
        errors.singledate = 'singledate is required'
    }
    return errors
}

export const EmailTemplateValidation = values => {
    console.log("valuesvaluesvaluesvalues")
    console.log(values)
    const errors = {}
    if (!values.UserName) {
        errors.UserName = 'UserName is required'
    }
    if (!values.MobileNumber) {
        errors.MobileNumber = 'MobileNumber is required'
    }
    if (!values.selectbox) {
        errors.selectbox = 'selectbox is required'
    }
    if (!values.selectboxmulti) {
        errors.selectboxmulti = 'multi selectbox is required'
    }
    // if(!values.gender){
    //     errors.gender='Gender is required'
    // }
    return errors
}