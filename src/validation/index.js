export const LoginValidation = values => {
    const errors = {}

    if (!values.email) {
      errors.email = 'Enter Your Email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    if (!values.password) {
      errors.password = 'Enter Your Password'
    }
    return errors;
}

export const RegisterValidation = values => {
  const errors = {}
  if (!values.first_name) {
     errors.first_name = 'Enter Your First Name'
  }
  if (!values.last_name) {
    errors.last_name = 'Enter Your Last Name'
  }
  if (!values.email) {
    errors.email = 'Enter Your EMail'
  }
  if (!values.mobile_number) {
    errors.mobile_number = 'Enter Your Mobile Number'
  }
  if (!values.password) {
    errors.password = 'Enter Your Password'
  }
  return errors
}

