import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { BrowserRouter } from 'react-router-dom'
import AppStore from './Store';
import WebRouter from './Router';
import './App.css';
import history from './helpers/history'

class App extends Component {
  render() {
    return <Provider store={AppStore}>
      <BrowserRouter history={history}>
        <WebRouter />
      </BrowserRouter>
    </Provider>
  }
}
export default App;