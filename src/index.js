import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import "./assets/css/index.scss";
import App from './App';
import register from './serviceWorker';
import "./assets/css/bootstrap.min.css"
import "./assets/css/unicons.css"
import "./assets/css/owl.carousel.min.css"
import "./assets/css/owl.theme.default.min.css"
import "./assets/css/tooplate-style.css"
import "./assets/css/common_style.scss"


ReactDOM.render(
    <App />,
    document.getElementById('root')
);

// Truehoch Register();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
