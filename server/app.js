require('dotenv').config()
const express = require("express"), 
bodyParser = require("body-parser"),
cors = require("cors"),
path = require('path'),
db = require("./models"),
indexRouter = require('./routes/index'),
app = express();

var corsOptions = {
  origin: "http://localhost:3000"
};
app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

db.sequelize.sync();

app.use('/api', indexRouter);

app.use(express.static(path.join(__dirname, '../build')));
// app.use(express.static(path.join(__dirname, '../public')));

// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to server application." });
// });

// set port, listen for requests
// const PORT = process.env.PORT || 8080;
// app.listen(PORT, () => {
//   console.log(`Server is running on port ${PORT}.`);
// });

app.get('/*', function (request, res) {
  res.sendFile(path.join(__dirname, '../build/index.html'), function (err) {
      if (err) {
          res.status(500).send(err)
      }
  });
})

module.exports = app;