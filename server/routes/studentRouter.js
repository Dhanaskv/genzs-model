const express = require('express');
const router = express.Router();
const student = require('../controllers/student');

router.get("/all", student.findAll);
router.get("/findOneStudent/:id", student.findOneStudent);
router.post("/newStudent",student.createStudent)
router.post("/updateStudent",student.updateStudent)

module.exports = router;