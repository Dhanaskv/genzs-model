const express = require('express');
const router =  express.Router();
const users = require('../controllers/user');

router.post('/login',users.login_verfication);
router.post('/emailAlreadyExits',users.emailAlreadyExits);
router.post('/validateToken',users.check_token);

module.exports = router;