const express = require('express');
const router = express.Router();
const organization = require('../controllers/organization');

router.post('/createOrganization', organization.createOrganisation);
router.post('/updateOrganization', organization.updateOrganization);
router.get("/all", organization.findAll);
router.get('/organizationById/:id', organization.findById);
router.post('/updateOrgStatus', organization.statusUpdateOrg);

module.exports = router;
