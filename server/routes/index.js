const express = require('express');
const router = express.Router();

/***** List of Router Controllers ****/
const studentRouter = require('./studentRouter');
const opportunity = require('./opportunity');
const organizationRouter = require('./organization')
const schoolDistick = require('./schooldistick')
const users = require('./users')
const studentApplied = require('./studentAppliedRouter')
const adminRouter = require('./admin');

router.use('/admin', adminRouter);
router.use('/student', studentRouter)
router.use('/opportunity', opportunity)
router.use('/organization', organizationRouter)
router.use('/users', users);
router.use('/schooldistick', schoolDistick);
router.use('/studentApplied', studentApplied);

module.exports = router;