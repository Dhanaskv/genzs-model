const express = require('express');
const router = express.Router();
const studentApplied = require('../controllers/studentApplied');

router.get("/all/:id", studentApplied.findAll);
router.post("/newApplied",studentApplied.createApplied)
// router.get("/applied", student.findApplied);
// router.post("/updateStudent",student.updateStudent)

module.exports = router;