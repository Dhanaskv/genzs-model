const express = require('express');
const router = express.Router();
const opportunity = require("../controllers/opportunties");

router.get("/all", opportunity.findAll);
router.get("/getOne/:id", opportunity.findOneOpportunity);
router.get("/findOneOpportunityId/:id", opportunity.findOneOpportunityId);
router.post("/findStudentId", opportunity.findStudentId);
router.post("/newOpportunity", opportunity.createOpportunity);
router.post("/updateOpportunity", opportunity.updateOpportunity);


module.exports = router;

