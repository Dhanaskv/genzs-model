const express = require('express');
const router = express.Router();
const admin = require('../controllers/admin');

router.post('/adminById', admin.adminById);

module.exports = router;