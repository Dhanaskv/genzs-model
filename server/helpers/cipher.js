const crypto = require('crypto-js');

/* encrypt plain text to encrypted text */
exports.encrypt = value => crypto.AES.encrypt(value, process.env.SECRET_KEY).toString();

/* Decoding the encrypted text */
exports.decrypt = value => crypto.AES.decrypt(value, process.env.SECRET_KEY).toString(crypto.enc.Utf8);

