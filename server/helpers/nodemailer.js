var nodemailer = require('nodemailer');
const { SMTP_HOST, SMTP_USER_NAME, SMTP_PASSWORD, SMTP_PORT } = require('./config')

var transporter = nodemailer.createTransport({
    host: SMTP_HOST,
    port: 25, // secure port 465
    secureConnection: false,
    auth: {
        user: SMTP_USER_NAME,
        pass: SMTP_PASSWORD
    }
});

const default_mail = 'akash.n@truehoch.com';

exports.sendMail = async function (to_mail, subject, email_content) {
    try {

        console.log('sendmail coming in node mail')
        var mailOptions = {
            from: SMTP_USER_NAME,
            to: to_mail,
            subject: subject,
            html: email_content
        };

        return new Promise((resolve, reject) => {
            console.log('sendmail coming in errprrrrr')
            transporter.sendMail(mailOptions, (error, info) => {
                console.log('error coming in errprrrrr', error)
                if (error !== null) {
                    reject({ isMailSent: 0, message: error })
                }
                resolve({ isMailSent: 1, message: 'Sent' })
            });
        })
    } catch (error) {
        console.log('error while sending mail', error)
    }
};