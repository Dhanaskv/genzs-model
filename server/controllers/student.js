const db = require("../models"); // models path depend on your structure
const moment = require('moment');
const Sequelize = require("sequelize");
const { sequelize } = require("../models");

const { student, users, schooldistrict } = db;
const { encrypt } = require('../helpers/cipher');


exports.findAll = (req, res) => {
  student.findAll().then(data => { res.status(200).send(data) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials."
      })
    })
}

exports.findOneStudent = (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: SSSSSSSS', req.params);

  sequelize.query(`SELECT  student.id, student.FirstName, student.LastName, student.Email, student.Grade, student.SchoolName, 
  student.SchoolDistrict, student.Mobile, student.DOB, student.RegisteredDate, student.CreatedOn, student.CreatedBy,
   student.UpdatdBy, student.UpdatedOn, DistrictName, DATE_FORMAT(FROM_DAYS(DATEDIFF(CURRENT_DATE(),DOB)), '%Y')+0 AS studentAge from student
   INNER  JOIN genzs.schooldistrict ON student.SchoolDistrict = schooldistrict.Id
   where student.id = 40`).then(data => { res.status(200).send(data[0]) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials."
      })
    })
}

exports.createStudent = async (req, res) => {
  const { FirstName, LastName, Email, Grade, SchoolName, PhoneNo, DOB, Password, DomainName } = req.body;
  try {
    let encryptPassword = encrypt(Password);
    const sch_dist = await schooldistrict.findAll({
      where: {
        DomainName: DomainName
      }
    });

    if (sch_dist.length > 0) {
      const _stu_response = await student.create({
        FirstName: FirstName,
        LastName: LastName,
        Email: Email,
        Grade: Grade,
        SchoolName: SchoolName,
        SchoolDistrict: sch_dist[0].dataValues.id,
        Mobile: PhoneNo,
        DOB: DOB,
        RegisteredDate: moment().format('YYYY-MM-DD HH:mm:ss'),
        CreatedOn: moment().format('YYYY-MM-DD HH:mm:ss'),
        CreatedBy: FirstName,
        UpdatdBy: FirstName,
      });

      const user_response = await users.create({
        UserName: Email,
        Password: encryptPassword,
        IsActive: 1,
        RoleId: 3,
        StudentId: _stu_response.dataValues.id,
        CreatedOn: moment().format('YYYY-MM-DD HH:mm:ss'),
        CreatedBy: FirstName,
        UpdatdBy: FirstName,
      });

      if (_stu_response && user_response) {
        res.status(200).send({ success: 'true', data: _stu_response, user_response, sch_dist })
      } else {
        res.status(500).send({
          message: "Error While inserting Student Registration"
        })
      }
    } else {
      res.status(200).send({ emailNValid: 1 })
    }

  } catch (error) {
    res.status(500).send({
      message: error.message || "Error While inserting Student Registration"
    })
  }
}

exports.updateStudent = async (req, res) => {

  console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.body);
  const { FirstName, LastName, Email, Grade, SchoolName, PhoneNo, DOB, SchoolDistrict, DomainName } = req.body;

  try {
    const opp_response = await student.update({
      FirstName: FirstName,
      LastName: LastName,
      Email: Email,
      Grade: Grade,
      SchoolName: SchoolName,
      SchoolDistrict: SchoolDistrict,
      Mobile: PhoneNo,
      DOB: DOB,
      CreatedBy: FirstName,
      UpdatdBy: FirstName,
    }, {
      where: { Id: req.body.Id }

    });

    if (opp_response) {
      res.status(200).send({ success: true, data: opp_response })
    } else {
      res.status(500).send({
        message: "Error While inserting Organisation Registration"
      })
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || "Error While inserting Organisation Registration"

    })
  }
};