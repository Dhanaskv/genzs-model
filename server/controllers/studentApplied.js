const db = require("../models"); // models path depend on your structure
const moment = require('moment');
const Sequelize = require("sequelize");
const { sequelize } = require("../models");
let currentDate = moment().format('YYYY-MM-DD HH:mm:ss');

const { opportunityapplications } = db;

exports.findAll = (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: student', req.params.id);
  
  sequelize.query(`select opportunityapplications.Id, opportunityapplications.OpportunityId, opportunityapplications.StudentId,
    opportunityapplications.Status, opportunityapplications.CreatedOn, Grade,  opportunityapplications.CreatedBy, opportunityapplications.UpdatedOn,
    opportunityapplications.UpdatdBy, FirstName, Email,SchoolName,Mobile, DATE_FORMAT(FROM_DAYS(DATEDIFF(CURRENT_DATE(),DOB)), '%Y')+0 AS studentAge 
    from opportunityapplications
    INNER  JOIN genzs.student ON opportunityapplications.StudentId = student.Id
    where Status = 1 and OpportunityId = ${req.params.id};`).then(data => { res.status(200).send(data[0]) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials."
      })
    })
}

exports.createApplied = async (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.body);

  const { OpportunityId, StudentId, Status } = req.body;

  try {
    const opp_response = await opportunityapplications.create({
      OpportunityId: OpportunityId,
      StudentId: StudentId,
      Status: Status,
      CreatedBy:StudentId,
      UpdatdBy : OpportunityId,
      CreatedOn: currentDate,
      UpdatedOn: currentDate,

    });

    if (opp_response) {
      res.status(200).send({ success: true, data: opp_response })
    } else {
      res.status(500).send({
        message: "Error While inserting Organisation Registration"
      })
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || "Error While inserting Organisation Registration"
    })
  }
};

