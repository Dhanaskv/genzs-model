const db = require('../models');
const SchoolDistrict = db.schooldistrict
const moment = require('moment');
let currentDate = moment().format('YYYY-MM-DD HH:mm:ss');

exports.findAll = (req, res) => {
    SchoolDistrict.findAll().then(data => { res.status(200).send(data) })
    
        .catch(error => {
          res.status(500).send({
            message: error.message || "Some error occurred while retrieving tutorials."
          })
        });
    
    };
