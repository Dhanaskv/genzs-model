const db = require('../models');
const moment = require('moment');
const organization = db.organization;
const users = db.users;
const emailTemplate = db.emailTemplate;
let currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
const nodemailer = require('../helpers/nodemailer');
const { encrypt } = require('../helpers/cipher');

exports.findAll = (req, res) => {
    organization.findAll().then(data => { res.status(200).send(data) })

        .catch(error => {
            res.status(500).send({
                message: error.message || "Some error occurred while retrieving tutorials."
            })
        });

};


exports.findById = async (req, res) => {
    const { id } = req.params;
    try {
        const ngoById = await organization.findAll({
            where: {
                id: id
            }
        });
        res.status(200).send({ success: 'true', data: ngoById })
    } catch (error) {
        console.log("error while fetching organization by id", error)
    }

}


exports.createOrganisation = async (req, res) => {
    const { FirstName, LastName, Role, Email, Password, PhoneNo, OrganizationName, Website, OfficialEmail, ShortSummary, AboutUs, Address } = req.body;
    try {
        let encryptPassword = encrypt(Password);
        const org_response = await organization.create({
            Organizationname: OrganizationName,
            Website: Website,
            OfficialEmail: OfficialEmail,
            ShortSummary: ShortSummary,
            AboutUs: AboutUs,
            Address: Address,
            RegisteredDate: currentDate,
            Status: 2,
            ContactPersonFirstName: FirstName,
            // ContactPersonLastName: LastName,
            ContactPersonEmail: Email,
            ContactPersonMobileNumber: PhoneNo,
            ContactPersonRole: Role,
            CreatedOn: currentDate,
            CreatedBy: FirstName,
            UpdatdBy: FirstName,
        });

        const user_response = await users.create({
            UserName: Email,
            Password: encryptPassword,
            IsActive: 1,
            RoleId: 2,
            CreatedOn: moment().format('YYYY-MM-DD HH:mm:ss'),
            OrganizationId: org_response.dataValues.id,
            CreatedOn: moment().format('YYYY-MM-DD HH:mm:ss'),
            CreatedBy: FirstName,
            UpdatdBy: FirstName,
        });

        if (org_response && user_response) {
            let email = await emailTemplate.findAll({
                where: {
                    id: 1
                }
            });
            let emailtemp = email[0].dataValues;
            let emailContent = emailtemp.Content;
            let emailSubject = emailtemp.Subject;

            let content = emailContent;

            content = content.replace("{Name}", FirstName + ' ' + LastName);
            content = content.replace("{Role}", Role);
            content = content.replace("{Email}", Email);
            content = content.replace("{phoneNo}", PhoneNo);
            content = content.replace("{organizationName}", OrganizationName);
            content = content.replace("{Website}", Website);
            content = content.replace("{officialEmail}", OfficialEmail);
            content = content.replace("{shortSummary}", ShortSummary);
            content = content.replace("{aboutUs}", AboutUs);
            content = content.replace("{address}", Address);


            let fn_admin = await users.findAll({
                where: {
                    RoleId: 1
                }
            });
            let tomail = [];
            for (let index = 0; index < fn_admin.length; index++) {
                let email = fn_admin[index].dataValues.UserName;
                tomail.push(email.toString());
            };

            let emailStatus = ''
            nodemailer.sendMail(tomail, emailSubject, content).then(async function (isMailSent, message) {
                emailStatus = isMailSent.message;

                if (!isMailSent) {
                    emailStatus = message;
                }

            }, (error) => {
                setTimeout(() => {
                    emailStatus = isMailSent.message;
                }, 20000);
            });
            res.status(200).send({ success: true, data: org_response, user_response, emailStatus })
        } else {
            res.status(500).send({
                message: "Error While inserting Organisation Registration"
            })
        }
    } catch (error) {
        res.status(500).send({
            message: error.message || "Error While inserting Organisation Registration"
        })
    }
};


exports.statusUpdateOrg = async (req, res) => {
    try {
        const { id, Status, UpdatedBy, RejectedReason, registerEmail, userName } = req.body;
        console.log('reqested params', req.body);
        const updateUser = await organization.update({
            UpdatdBy: UpdatedBy,
            Status: Status,
            RejectedReason: RejectedReason || null
        }, {
            where: {
                Id: id
            }
        });
        let emailStatus = ''
        if (RejectedReason) {
            let email = await emailTemplate.findAll({
                where: {
                    id: 3
                }
            });
            let emailtemp = email[0].dataValues;
            let emailContent = emailtemp.Content;
            let emailSubject = emailtemp.Subject;

            let content = emailContent;

            content = content.replace("{rejectedReason}", RejectedReason);

            let tomail = 'akash.n@truehoch.com' //registerEmail;

            nodemailer.sendMail(tomail, emailSubject, content).then(async function (isMailSent, message) {
                emailStatus = isMailSent.message;

                if (!isMailSent) {
                    emailStatus = message;
                }

            }, (error) => {
                setTimeout(() => {
                    emailStatus = isMailSent.message;
                }, 2000);
            });

        } else {
            let email = await emailTemplate.findAll({
                where: {
                    id: 2
                }
            });
            let emailtemp = email[0].dataValues;
            let emailContent = emailtemp.Content;
            let emailSubject = emailtemp.Subject;

            let content = emailContent;

            content = content.replace("{name}", userName);

            let tomail = 'akash.n@truehoch.com' //registerEmail;

            nodemailer.sendMail(tomail, emailSubject, content).then(async function (isMailSent, message) {
                emailStatus = isMailSent.message;

                if (!isMailSent) {
                    emailStatus = message;
                }

            }, (error) => {
                setTimeout(() => {
                    emailStatus = isMailSent.message;
                }, 2000);
            });
        }

        res.status(200).send({ success: true, data: updateUser, emailStatus });
    } catch (error) {
        console.log('error while updation Organization status', error)
    }
}
exports.updateOrganization = async (req, res) => {

    console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.body);
    const { FirstName, Role, Email, PhoneNo, OrganizationName, Website, OfficialEmail, ShortSummary, AboutUs, Address } = req.body;


    try {
        const opp_response = await organization.update({
            OrganizationName: OrganizationName,
            Website: Website,
            OfficialEmail: OfficialEmail,
            ShortSummary: ShortSummary,
            AboutUs: AboutUs,
            Address: Address,
            FirstName: FirstName,
            ContactPersonRole: Role,
            Email: Email,
            PhoneNo: PhoneNo,
        }, {
            where: { Id: req.body.Id }

        });

        if (opp_response) {
            res.status(200).send({ success: true, data: opp_response })
        } else {
            res.status(500).send({
                message: "Error While inserting Organisation Registration"
            })
        }
    } catch (error) {
        res.status(500).send({
            message: error.message || "Error While inserting Organisation Registration"

        })
    }
};
