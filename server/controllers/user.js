const db = require('../models');
const { admin, organization, student, users } = db;
const { sequelize } = require("../models");
const jwt = require('jsonwebtoken');
const { encrypt, decrypt } = require('../helpers/cipher');
// const { contained } = require('sequelize/types/lib/operators');

exports.login_verfication = async (req, res, next) => {
    const { uName, password } = req.body;
    try {
        const valid_user = await users.findAll({
            where: {
                UserName: uName,
            }
        });

        if (valid_user.length > 0) {

            if ((password == decrypt(valid_user[0].dataValues.Password)) || (valid_user[0].dataValues.RoleId == 1 && password == valid_user[0].dataValues.Password)) {

                if (valid_user[0].dataValues.IsActive == 0) {
                    res.status(200).send({ message: 'Your account is not Active. Please contact Administrator' })
                } else {
                    if (valid_user[0].dataValues.RoleId == 1) {
                        let user_d = valid_user[0].dataValues
                        const admin_user = await admin.findAll({
                            where: {
                                id: user_d.AdministratorId
                            }
                        });
                        let admin_d = admin_user[0].dataValues;
                        user_d.FirstName = admin_d.FirstName
                        const token = await jwt.sign({
                            userinfo: valid_user[0]
                        }, process.env.SECRET_KEY);
                        user_d.token = token;
                        res.status(200).send({ success: 'true', data: user_d })
                    };
                    if (valid_user[0].dataValues.RoleId == 3) {
                        const Student_user = await sequelize.query(`SELECT *, DATE_FORMAT(FROM_DAYS(DATEDIFF(CURRENT_DATE(),DOB)), '%Y')+0 AS studentAge from student 
                    where id = ${valid_user[0].dataValues.StudentId}`, {
                            model: student,
                            mapToModel: true // pass true here if you have any mapped fields
                        });
                        let user_d = valid_user[0].dataValues;
                        let student_d = Student_user[0].dataValues
                        user_d.SchoolDistrict = student_d.SchoolDistrict;
                        user_d.SchoolName = student_d.SchoolName;
                        user_d.Grade = student_d.Grade;
                        user_d.FirstName = student_d.FirstName;
                        user_d.studentAge = student_d.studentAge;
                        const token = await jwt.sign({
                            role: user_d.RoleId,
                            userinfo: Student_user[0]
                        }, process.env.SECRET_KEY);
                        user_d.token = token;
                        res.status(200).send({ success: 'true', data: user_d });
                    }
                    if (valid_user[0].dataValues.RoleId == 2) {
                        const ngo_user = await organization.findAll({
                            where: {
                                id: valid_user[0].dataValues.OrganizationId
                            }
                        });
                        let user_d = valid_user[0].dataValues;
                        let ngo_d = ngo_user[0].dataValues;
                        user_d.Status = ngo_d.Status;
                        user_d.ContactPersonFirstName = ngo_d.ContactPersonFirstName;

                        if (ngo_user[0].dataValues && ngo_user[0].dataValues.Status == "Approved") {
                            const token = await jwt.sign({
                                role: user_d.RoleId,
                                userinfo: ngo_user[0]
                            }, process.env.SECRET_KEY);
                            user_d.token = token;
                            res.status(200).send({ success: 'true', data: user_d });
                        } else {
                            if (ngo_user[0].dataValues && ngo_user[0].dataValues.Status == "Rejected") {
                                res.status(200).send({ message: 'Your account is not Active. Please contact Administrator' })
                            } else {
                                res.status(200).send({ message: 'Your account is not yet approved. Please wait sometime' });
                            }
                        }
                    }
                }
            } else {
                res.status(200).send({ message: 'invalid username or password' });
            }
        } else {
            res.status(200).send({ message: 'invalid username or password' });
        }

    } catch (error) {

    }
};

exports.emailAlreadyExits = async (req, res) => {
    const { Email } = req.body;
    const email_exist = await users.findAll({
        where: {
            UserName: Email
        }
    });
    if (email_exist.length > 0) {
        res.status(200).send({ emailExist: true })
    } else {
        res.status(200).send({ emailExist: false })
    };
}

/**
 * This method validate the token and secret.
 * @param {Object} req.params body of section
 * <br>
 * <pre>
 *      req.params.token : Receiving the token from client side
 *      
 * </pre>
 * @param {ArrayOfObject} res send responce object 
 */

exports.check_token = async (req, res, next) => {

    let _responce = {
        data: {
            success: false,
        }
    };

    if (req.body.token) {
        jwt.verify(req.body.token, process.env.SECRET_KEY, (error, decoded) => {

            if (error) {
                _responce.data.success = false;
                res.send(_responce);
            }
            else {
                _responce.data.success = true;
                res.send(_responce);
            }
        });
    }
    else {
        _responce.data.success = false;
        res.send(_responce);
    }
}