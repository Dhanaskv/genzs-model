const db = require('../models');
const opportunity = db.opportunity
const organization = db.organization;
const { sequelize } = require("../models");
const moment = require('moment');
let currentDate = moment().format('YYYY-MM-DD HH:mm:ss');



exports.findAll = (req, res) => {
  opportunity.findAll().then(data => { res.status(200).send(data) })

    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials."
      })
    });

};



exports.findOneOpportunityId = (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: opp_', req.params);
  sequelize.query(`SELECT opportunity.Id, opportunity.OpportunitySubject, opportunity.OrganizationId, opportunity.AgeLimitStart, opportunity.AgeLimitEnd, opportunity.Status,
  opportunity.RequiredPosition, opportunity.OpportunityDescription, opportunity.OpportunityDistrict, opportunity.PublishDate, opportunity.CreatedOn,
  opportunity.CreatedBy, opportunity.UpdatedOn, opportunity.UpdatdBy, Organizationname, DistrictName
 FROM genzs.opportunity 
 INNER  JOIN genzs.organizations ON opportunity.OrganizationId = organizations.Id
 INNER  JOIN genzs.schooldistrict ON opportunity.OpportunityDistrict = schooldistrict.Id where OrganizationId=${req.params.id}`).then(data => { res.status(200).send(data[0]) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials." + id
      });
    });
};

exports.findStudentId = (req, res) => {
  const { SchoolDistrict, studentAge } = req.body;
  sequelize.query(`SELECT opportunity.Id, opportunity.OpportunitySubject, opportunity.OrganizationId, opportunity.AgeLimitStart, opportunity.AgeLimitEnd, opportunity.Status,
  opportunity.RequiredPosition, opportunity.OpportunityDescription, opportunity.OpportunityDistrict, opportunity.PublishDate, opportunity.CreatedOn,
  opportunity.CreatedBy, opportunity.UpdatedOn, opportunity.UpdatdBy, Organizationname, DistrictName
 FROM genzs.opportunity 
 INNER JOIN genzs.organizations ON opportunity.OrganizationId = organizations.Id
 INNER JOIN genzs.schooldistrict ON opportunity.OpportunityDistrict = schooldistrict.Id
 where opportunity.OpportunityDistrict = ${SchoolDistrict} and opportunity.Status = 1 and opportunity.AgeLimitStart <= ${studentAge} and opportunity.AgeLimitEnd >= ${studentAge}`).then(data => { res.status(200).send(data[0]) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials."
      });
    });
};

exports.findOneOpportunity = (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.params);

  sequelize.query(`SELECT opportunity.Id, opportunity.OpportunitySubject, opportunity.OrganizationId, opportunity.AgeLimitStart, opportunity.AgeLimitEnd, opportunity.Status,
  opportunity.RequiredPosition, opportunity.OpportunityDescription, opportunity.OpportunityDistrict, opportunity.PublishDate, opportunity.CreatedOn,
  opportunity.CreatedBy, opportunity.UpdatedOn, opportunity.UpdatdBy, Organizationname, DistrictName
 FROM genzs.opportunity 
 INNER  JOIN genzs.organizations ON opportunity.OrganizationId = organizations.Id
 INNER  JOIN genzs.schooldistrict ON opportunity.OpportunityDistrict = schooldistrict.Id where opportunity.Id=${req.params.id}`).then(data => { res.status(200).send(data[0]) })

    // opportunity.findAll({ where: { Id: req.params.id } }).then(data => { res.status(200).send(data) })
    .catch(error => {
      res.status(500).send({
        message: error.message || "Some error occurred while retrieving tutorials." + id
      });
    });
};

exports.createOpportunity = async (req, res) => {
  console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.body);


  const { OpportunitySubject, Organization, AgeLimitStart, AgeLimitEnd, RequiredPosition, Status, OpportunityDescription } = req.body;

  try {
    const opp_response = await opportunity.create({
      OpportunitySubject: OpportunitySubject,
      OrganizationId: Organization,
      OpportunityDescription: OpportunityDescription,
      OpportunityDistrict: req.body.OpportunityDistrict.value,
      AgeLimitStart: AgeLimitStart,
      AgeLimitEnd: AgeLimitEnd,
      Status: req.body.Status.value,
      RequiredPosition: RequiredPosition,
      PublishDate: currentDate,
      CreatedOn: currentDate,
      UpdatedOn: currentDate,
      CreatedBy: Organization,
      UpdatdBy: Organization

    });

    if (opp_response) {
      res.status(200).send({ success: true, data: opp_response })
    } else {
      res.status(500).send({
        message: "Error While inserting Organisation Registration"
      })
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || "Error While inserting Organisation Registration"
    })
  }
};

exports.updateOpportunity = async (req, res) => {

  console.log('login the createOpportunity:::::::::::::::::::: opportunity', req.body);
  const { OpportunitySubject, OpportunityDescription, OpportunityDistrict, OrganizationId, AgeLimitStart, AgeLimitEnd, RequiredPosition, } = req.body;

  try {
    const opp_response = await opportunity.update({
      OpportunitySubject: OpportunitySubject,
      OrganizationId: OrganizationId,
      OpportunityDescription: OpportunityDescription,
      OpportunityDistrict: req.body.OpportunityDistrict.value,
      AgeLimitStart: AgeLimitStart,
      AgeLimitEnd: AgeLimitEnd,
      Status: req.body.Status.value,
      RequiredPosition: RequiredPosition,
      PublishDate: currentDate,
      CreatedOn: currentDate,
      UpdatedOn: currentDate,
      UpdatdBy: OrganizationId
    }, {
      where: { Id: req.body.Id }

    });

    if (opp_response) {
      res.status(200).send({ success: true, data: opp_response })
    } else {
      res.status(500).send({
        message: "Error While inserting Organisation Registration"
      })
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || "Error While inserting Organisation Registration"

    })
  }
};




