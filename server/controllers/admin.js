const db = require('../models');
const { admin } = db;

exports.adminById = async (req, res) => {
    const { adminId } = req.body;
    try {
        const admin_d = await admin.findAll({
            where: {
                id: adminId
            }
        });

        res.status(200).send({ data: admin_d });
    } catch (error) {
        console.log('Error while fetch admin by id', error)
    }
};