module.exports = (sequelize, Sequelize) => {

    // Id, OpportunitySubject, OrganizationId, AgeLimitStart, AgeLimitEnd, Status, RequiredPosition, OpportunityDescription,
    // OpportunityDistrict, PublishDate, CreatedOn, CreatedBy, UpdatedOn, UpdatdBy

    const opportunity = sequelize.define("opportunity", {
        OpportunitySubject: { type: Sequelize.STRING },
        OrganizationId: { type: Sequelize.INTEGER },
        AgeLimitStart: { type: Sequelize.INTEGER },
        AgeLimitEnd: { type: Sequelize.INTEGER },
        OpportunityDescription: { type: Sequelize.STRING },
        OpportunityDistrict: { type: Sequelize.STRING },
        Status: { type: Sequelize.ENUM('Active', 'Expired') },
        PublishDate: { type: Sequelize.DATEONLY },
        RequiredPosition: { type: Sequelize.INTEGER },
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatedOn: { type: Sequelize.DATE },
        UpdatdBy: { type: Sequelize.STRING }

    }, { freezeTableName: true, timestamps: false });


    return opportunity;
};