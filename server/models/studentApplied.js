module.exports = (sequelize, Sequelize) => {


    
    const student = sequelize.define("opportunityapplications", {
        OpportunityId: { type: Sequelize.INTEGER },
        StudentId: { type: Sequelize.INTEGER },
        Status: {type: Sequelize.ENUM('Applied', 'Approved','Rejected')},
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatdBy: { type: Sequelize.STRING },


    }, { freezeTableName: true, timestamps: false });

    return student;
};