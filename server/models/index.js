const dbConfig = require("../db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.opportunity = require("./opportunity.js")(sequelize, Sequelize);
db.student = require('./student.js')(sequelize, Sequelize);
db.organization = require('./organization')(sequelize, Sequelize);
db.users = require('./users')(sequelize, Sequelize);
db.emailTemplate = require('./emailTemplate')(sequelize, Sequelize);
db.schooldistrict = require('./schooldistrict')(sequelize, Sequelize);
db.opportunityapplications = require('./studentApplied')(sequelize, Sequelize)
db.admin = require('./admin')(sequelize, Sequelize);

module.exports = db;
