module.exports = (sequelize, Sequelize) => {
    const emailTemplate = sequelize.define('emailtemplate', {
        EmailTemplateName: { type: Sequelize.STRING },
        Subject: { type: Sequelize.STRING },
        Content: { type: Sequelize.TEXT },
        Type: { type: Sequelize.STRING },
        CreatedDate: { type: Sequelize.DATE },
        LastUpdatedDate: { type: Sequelize.DATE }
    }, { freezeTableName: true, timestamps: false });
    return emailTemplate;
}