
module.exports = (sequelize, Sequelize) => {
    const Organization = sequelize.define("organizations", {
        Organizationname: { type: Sequelize.STRING },
        Website: { type: Sequelize.STRING },
        OfficialEmail: { type: Sequelize.STRING },
        ShortSummary: { type: Sequelize.STRING },
        AboutUs: { type: Sequelize.TEXT },
        Address: { type: Sequelize.STRING },
        RegisteredDate: { type: Sequelize.DATE },
        Status: { type: Sequelize.ENUM('Requested', 'Approved', 'Rejected') },
        ContactPersonFirstName: { type: Sequelize.STRING },
        // ContactPersonLastName: { type: Sequelize.STRING },
        // RejectedReason: { type: Sequelize.STRING },
        ContactPersonEmail: { type: Sequelize.STRING },
        ContactPersonMobileNumber: { type: Sequelize.STRING },
        ContactPersonRole: { type: Sequelize.STRING },
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatedOn: { type: Sequelize.DATE },
        UpdatdBy: { type: Sequelize.STRING },
    }, { freezeTableName: true, timestamps: false });
    return Organization;
}