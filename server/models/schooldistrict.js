module.exports = (sequelize, Sequelize) => {
    const schooldistrict = sequelize.define('schooldistrict', {
        DistrictName: { type: Sequelize.STRING },
        DomainName: { type: Sequelize.STRING },
        IsActive: { type: Sequelize.INTEGER },
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatedOn: { type: Sequelize.DATE },
        UpdatdBy: { type: Sequelize.STRING }
    }, { freezeTableName: true, timestamps: false });
    return schooldistrict;
}