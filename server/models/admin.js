module.exports = (sequelize, Sequelize) => {
    const admin = sequelize.define('administrators', {
        FirstName: { type: Sequelize.STRING },
        LastName: { type: Sequelize.STRING },
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatedOn: { type: Sequelize.DATE },
        UpdatdBy: { type: Sequelize.STRING }
    }, { freezeTableName: true, timestamps: false });
    return admin;
}