module.exports = (sequelize, Sequelize) => {

    const student = sequelize.define("student", {
        FirstName: { type: Sequelize.STRING },
        LastName: { type: Sequelize.STRING },
        Email: { type: Sequelize.STRING },
        Grade: { type: Sequelize.STRING },
        SchoolName: { type: Sequelize.STRING },
        SchoolDistrict: { type: Sequelize.INTEGER },
        Mobile: { type: Sequelize.STRING },
        DOB: { type: Sequelize.DATEONLY },
        RegisteredDate: { type: Sequelize.DATE },
        CreatedOn: { type: Sequelize.DATE },
        CreatedBy: { type: Sequelize.STRING },
        UpdatdBy: { type: Sequelize.STRING },
    }, { freezeTableName: true, timestamps: false });

    return student;
};