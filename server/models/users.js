module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("users", {
        UserName : { type: Sequelize.STRING },
        Password : { type: Sequelize.STRING },
        IsActive : { type: Sequelize.INTEGER },
        RoleId : { type: Sequelize.INTEGER },
        AdministratorId : { type: Sequelize.INTEGER },
        OrganizationId : { type: Sequelize.INTEGER },
        StudentId : { type: Sequelize.INTEGER },
        CreatedOn : { type: Sequelize.DATE },
        CreatedBy : { type: Sequelize.STRING },
        UpdatedOn : { type: Sequelize.DATE },
        UpdatdBy : { type: Sequelize.STRING }
    }, { freezeTableName: true, timestamps: false });
    return Users;
}
